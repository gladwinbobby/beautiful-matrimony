package in.billiontags.beautifulmatrimony;

import static in.billiontags.beautifulmatrimony.app.Activity.launch;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;

    private ImageView mImageViewProfile, mImageViewMatrimony, mImageViewPackage, mImageViewAccepted,
            mImageViewRejected, mImageViewInfo, mImageViewSearch, mImageViewHome, mImageViewMatch,
            mImageViewChat;

    private FrameLayout mLayoutBottomBar;

    private int mLastSelectedTopBar, mLastSelectedBottomBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initObjects();
        initCallbacks();
        invalidateTopBar(2);
        invalidateBottomBar(3);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        if (v == mImageViewProfile) {
            invalidateTopBar(1);
        } else if (v == mImageViewMatrimony) {
            invalidateTopBar(2);
        } else if (v == mImageViewPackage) {
            invalidateTopBar(3);
        } else if (v == mImageViewAccepted) {
            invalidateTopBar(4);
        } else if (v == mImageViewRejected) {
            invalidateTopBar(5);
        } else if (v == mImageViewInfo) {
            invalidateBottomBar(1);
        } else if (v == mImageViewSearch) {
            invalidateBottomBar(2);
        } else if (v == mImageViewHome) {
            invalidateBottomBar(3);
        } else if (v == mImageViewMatch) {
            invalidateBottomBar(4);
        } else if (v == mImageViewChat) {
            invalidateBottomBar(5);
        }
    }

    private void initObjects() {
        mImageViewProfile = (ImageView) findViewById(R.id.img_profile);
        mImageViewMatrimony = (ImageView) findViewById(R.id.img_matrimony);
        mImageViewPackage = (ImageView) findViewById(R.id.img_package);
        mImageViewAccepted = (ImageView) findViewById(R.id.img_accepted);
        mImageViewRejected = (ImageView) findViewById(R.id.img_rejected);
        mImageViewInfo = (ImageView) findViewById(R.id.img_info);
        mImageViewSearch = (ImageView) findViewById(R.id.img_search);
        mImageViewHome = (ImageView) findViewById(R.id.img_home);
        mImageViewMatch = (ImageView) findViewById(R.id.img_match);
        mImageViewChat = (ImageView) findViewById(R.id.img_chat);
        mLayoutBottomBar = (FrameLayout) findViewById(R.id.bottom_bar);

        mContext = this;
    }

    private void initCallbacks() {
        mImageViewProfile.setOnClickListener(this);
        mImageViewMatrimony.setOnClickListener(this);
        mImageViewPackage.setOnClickListener(this);
        mImageViewAccepted.setOnClickListener(this);
        mImageViewRejected.setOnClickListener(this);
        mImageViewInfo.setOnClickListener(this);
        mImageViewSearch.setOnClickListener(this);
        mImageViewHome.setOnClickListener(this);
        mImageViewMatch.setOnClickListener(this);
        mImageViewChat.setOnClickListener(this);
    }

    private void invalidateTopBar(int selected) {
        if (selected == mLastSelectedTopBar) {
            return;
        } else {
            disableLastSelectedTopBar();
            mLastSelectedTopBar = selected;
        }

        switch (selected) {
            case 1:
                mLayoutBottomBar.setVisibility(View.VISIBLE);
                setSelectedTopBar(mImageViewProfile);
                launch(mContext, ProfileActivity.class);
                break;
            case 2:
                mLayoutBottomBar.setVisibility(View.VISIBLE);
                setSelectedTopBar(mImageViewMatrimony);
                selectFragment(new MatrimonyFragment());
                break;
            case 3:
                mLayoutBottomBar.setVisibility(View.GONE);
                setSelectedTopBar(mImageViewPackage);
                selectFragment(new PackageFragment());
                break;
            case 4:
                mLayoutBottomBar.setVisibility(View.GONE);
                setSelectedTopBar(mImageViewAccepted);
                selectFragment(new AcceptedFragment());
                break;
            case 5:
                mLayoutBottomBar.setVisibility(View.GONE);
                setSelectedTopBar(mImageViewRejected);
                selectFragment(new RejectedFragment());
                break;
        }
    }

    private void disableLastSelectedTopBar() {
        switch (mLastSelectedTopBar) {
            case 1:
                setDisabledTopBar(mImageViewProfile);
                break;
            case 2:
                setDisabledTopBar(mImageViewMatrimony);
                break;
            case 3:
                setDisabledTopBar(mImageViewPackage);
                break;
            case 4:
                setDisabledTopBar(mImageViewAccepted);
                break;
            case 5:
                setDisabledTopBar(mImageViewRejected);
                break;
        }
    }

    private void setSelectedTopBar(ImageView view) {
        view.setBackgroundResource(R.drawable.bg_circle_selected_top_bar);
        view.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
    }

    private void setDisabledTopBar(ImageView view) {
        view.setBackgroundResource(R.drawable.bg_circle_disabled_top_bar);
        view.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
    }

    private void invalidateBottomBar(int selected) {
        if (selected == mLastSelectedBottomBar) {
            return;
        } else {
            disableLastSelectedBottomBar();
            mLastSelectedBottomBar = selected;
        }

        switch (selected) {
            case 1:
                setSelectedBottomBar(mImageViewInfo);
                launch(mContext, InfoActivity.class);
                break;
            case 2:
                setSelectedBottomBar(mImageViewSearch);
                selectFragment(new SearchFragment());
                break;
            case 3:
                setSelectedBottomBar(mImageViewHome);
                selectFragment(new MatrimonyFragment());
                break;
            case 4:
                setSelectedBottomBar(mImageViewMatch);
                selectFragment(new MatchFragment());
                break;
            case 5:
                setSelectedBottomBar(mImageViewChat);
                launch(mContext, ChatActivity.class);
                break;
        }
    }

    private void disableLastSelectedBottomBar() {
        switch (mLastSelectedBottomBar) {
            case 1:
                setDisabledBottomBar(mImageViewInfo);
                break;
            case 2:
                setDisabledBottomBar(mImageViewSearch);
                break;
            case 3:
                setDisabledBottomBar(mImageViewHome);
                break;
            case 4:
                setDisabledBottomBar(mImageViewMatch);
                break;
            case 5:
                setDisabledBottomBar(mImageViewChat);
                break;
        }
    }

    private void setSelectedBottomBar(ImageView view) {
        view.setBackgroundResource(R.drawable.bg_circle_selected_bottom_bar);
        view.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
    }

    private void setDisabledBottomBar(ImageView view) {
        view.setBackgroundResource(R.drawable.bg_circle_disabled_bottom_bar);
        view.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
    }

    private void selectFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }
}
