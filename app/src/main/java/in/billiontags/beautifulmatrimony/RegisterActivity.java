package in.billiontags.beautifulmatrimony;

import static in.billiontags.beautifulmatrimony.app.Activity.launchClearStack;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.Locale;

import in.billiontags.beautifulmatrimony.callback.RegisterCallback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends AppCompatActivity implements RegisterCallback {

    private static final int REGISTER = 1;
    private static final int OTP = 2;
    private static final int CURRENT_LOCATION = 3;
    private static final int RELIGION_DETAILS = 4;
    private static final int PERSONAL_DETAILS = 5;
    private static final int PROFESSIONAL_DETAILS = 6;
    private static final int ABOUT_ME = 7;
    private static final int PROFILE_PHOTO = 8;
    private static final int HOBBIES_INTERESTS = 9;
    private static final int BASIC_INFO = 10;
    private static final int LIFESTYLE_INFO = 11;
    private static final int RELIGION_INFO = 12;
    private static final int FAMILY_INFO = 13;
    private static final int PARTNER_PREFERENCE = 14;
    private static final int RELIGION_PREFERENCE = 15;
    private static final int LOCATION_PREFERENCE = 16;
    private static final int PROFESSION_PREFERENCE = 17;
    private static final int PARTNER_DESCRIPTION = 18;
    private Context mContext;
    private Toolbar mToolbar;
    private int mProgress = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initObjects();
        initToolbar();
        launchNextScreen();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.progress, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final MenuItem menuItem = menu.findItem(R.id.action_progress);
        FrameLayout layoutProgress = (FrameLayout) menuItem.getActionView();
        TextView textViewProgress = (TextView) layoutProgress.findViewById(R.id.txt_progress);
        textViewProgress.setText(
                String.format(Locale.getDefault(), getString(R.string.format_progress), mProgress));
        layoutProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void launchNextScreen() {
        processNextFragment();
        invalidateOptionsMenu();
        mProgress++;
    }

    private void initObjects() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        mContext = this;
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void processNextFragment() {
        switch (mProgress) {
            case REGISTER:
                setFragment(new CurrentLocationFragment());
                setToolbarTitle("Current Location");
                break;
            case OTP:
                setFragment(new CurrentLocationFragment());
                setToolbarTitle("Current Location");
                break;
            case CURRENT_LOCATION:
                setFragment(new ReligionDetailsFragment());
                setToolbarTitle("Religion Details");
                break;
            case RELIGION_DETAILS:
                setFragment(new PersonalDetailsFragment());
                setToolbarTitle("Personal Details");
                break;
            case PERSONAL_DETAILS:
                setFragment(new ProfessionalDetailsFragment());
                setToolbarTitle("Professional Details");
                break;
            case PROFESSIONAL_DETAILS:
                setFragment(new AboutFragment());
                setToolbarTitle("About You");
                break;
            case ABOUT_ME:
                setFragment(new HobbiesInterestsFragment());
                setToolbarTitle("Hobbies & Interests");
                break;
            case PROFILE_PHOTO:
                setFragment(new HobbiesInterestsFragment());
                setToolbarTitle("Hobbies & Interests");
                break;
            case HOBBIES_INTERESTS:
                setFragment(new BasicInformationFragment());
                setToolbarTitle("Basic Information");
                break;
            case BASIC_INFO:
                setFragment(new LifestyleInfoFragment());
                setToolbarTitle("Lifestyle Information");
                break;
            case LIFESTYLE_INFO:
                setFragment(new ReligionInfoFragment());
                setToolbarTitle("Religion Information");
                break;
            case RELIGION_INFO:
                setFragment(new FamilyInfoFragment());
                setToolbarTitle("Family Information");
                break;
            case FAMILY_INFO:
                setFragment(new BasicPartnerPreferenceFragment());
                setToolbarTitle("Partner Preferences");
                break;
            case PARTNER_PREFERENCE:
                setFragment(new ReligionPreferenceFragment());
                setToolbarTitle("Religion Preferences");
                break;
            case RELIGION_PREFERENCE:
                setFragment(new LocationPreferenceFragment());
                setToolbarTitle("Location Preferences");
                break;
            case LOCATION_PREFERENCE:
                setFragment(new ProfessionPreferenceFragment());
                setToolbarTitle("Profession Preferences");
                break;
            case PROFESSION_PREFERENCE:
                setFragment(new PartnerDescriptionFragment());
                setToolbarTitle("About Partner");
                break;
            case PARTNER_DESCRIPTION:
                launchClearStack(mContext, MainActivity.class);
                break;
            default:
                setFragment(new RegisterFragment());
                setToolbarTitle("Register");
                break;
        }
    }

    private void setToolbarTitle(String title) {
        mToolbar.setTitle(title);
    }

    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }
}
