package in.billiontags.beautifulmatrimony;


import static in.billiontags.beautifulmatrimony.app.Api.DATA_SIGN_UP;
import static in.billiontags.beautifulmatrimony.app.Api.GENDER_API_URL;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_COUNTRY_CODE;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_DOB;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_EMAIL;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_FIRST_NAME;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_GENDER;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_ID;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_LAST_NAME;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_MOTHER_TONGUE;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_NAME;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_PASSWORD;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_RELATIONSHIP;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_RELIGION;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_USERDETAILS;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_USERNAME;
import static in.billiontags.beautifulmatrimony.app.Api.REGISTER;
import static in.billiontags.beautifulmatrimony.app.Constant.GENDER_API_KEY;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.CountryPickerListener;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import in.billiontags.beautifulmatrimony.app.AppController;
import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.app.ToastBuilder;
import in.billiontags.beautifulmatrimony.app.VolleyErrorHandler;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;
import in.billiontags.beautifulmatrimony.model.MotherTongue;
import in.billiontags.beautifulmatrimony.model.Relationship;
import in.billiontags.beautifulmatrimony.model.Religion;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment implements View.OnClickListener,
        AdapterView.OnItemSelectedListener {

    private View mRootView;
    private Context mContext;
    private ProgressBar mProgressBar;
    private SearchableSpinner mSpinnerRelationship, mSpinnerReligion, mSpinnerMotherTongue;
    private EditText mEditTextFirstName, mEditTextLastName, mEditTextDob, mEditTextMobile,
            mEditTextEmail, mEditTextPass;
    private ImageView mImageViewCountry;
    private TextView mTextViewDialCode;
    private RadioGroup mGroupGender;
    private RadioButton mButtonMale, mButtonFemale;
    private Button mButtonContinue, mButtonRetry;
    private LinearLayout mLayoutRegister, mLayoutOops;
    private List<Relationship> mRelationshipList;
    private List<Religion> mReligionList;
    private List<MotherTongue> mMotherTongueList;
    private ArrayAdapter<Relationship> mRelationshipArrayAdapter;
    private ArrayAdapter<Religion> mReligionArrayAdapter;
    private ArrayAdapter<MotherTongue> mMotherTongueArrayAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;
    private String mCountryCode;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_register, container, false);
        initObjects();
        initCallbacks();
        initSpinners();
        displayLoading();
        getDataSignUp();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == mSpinnerRelationship) {
            Relationship relationship = mRelationshipList.get(
                    mSpinnerRelationship.getSelectedItemPosition());
            getGender(relationship.getRelationship());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        if (v == mEditTextDob) {
            promptDatePickerDialog();
        } else if (v == mImageViewCountry) {
            displayCountryPicker();
        } else if (v == mButtonContinue) {
            processRegister();
        } else if (v == mButtonRetry) {
            displayLoading();
            getDataSignUp();
        }
    }

    private void initObjects() {
        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progress);
        mSpinnerRelationship = (SearchableSpinner) mRootView.findViewById(R.id.spin_relationship);
        mSpinnerReligion = (SearchableSpinner) mRootView.findViewById(R.id.spin_religion);
        mSpinnerMotherTongue = (SearchableSpinner) mRootView.findViewById(R.id.spin_mother_tongue);
        mEditTextFirstName = (EditText) mRootView.findViewById(R.id.input_first_name);
        mEditTextLastName = (EditText) mRootView.findViewById(R.id.input_last_name);
        mEditTextDob = (EditText) mRootView.findViewById(R.id.input_dob);
        mEditTextMobile = (EditText) mRootView.findViewById(R.id.input_mobile);
        mEditTextEmail = (EditText) mRootView.findViewById(R.id.input_email);
        mEditTextPass = (EditText) mRootView.findViewById(R.id.input_pass);
        mImageViewCountry = (ImageView) mRootView.findViewById(R.id.img_country);
        mTextViewDialCode = (TextView) mRootView.findViewById(R.id.txt_dial_code);
        mGroupGender = (RadioGroup) mRootView.findViewById(R.id.grp_gender);
        mButtonMale = (RadioButton) mRootView.findViewById(R.id.radio_male);
        mButtonFemale = (RadioButton) mRootView.findViewById(R.id.radio_female);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);
        mButtonRetry = (Button) mRootView.findViewById(R.id.btn_retry);
        mLayoutRegister = (LinearLayout) mRootView.findViewById(R.id.register);
        mLayoutOops = (LinearLayout) mRootView.findViewById(R.id.oops);

        mContext = getActivity();
        mRelationshipList = new ArrayList<>();
        mReligionList = new ArrayList<>();
        mMotherTongueList = new ArrayList<>();
        mRelationshipArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mRelationshipList);
        mReligionArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mReligionList);
        mMotherTongueArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mMotherTongueList);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mSpinnerRelationship.setOnItemSelectedListener(this);
        mEditTextDob.setOnClickListener(this);
        mImageViewCountry.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
        mButtonRetry.setOnClickListener(this);
    }

    private void initSpinners() {
        mSpinnerRelationship.setTitle("Select Profile For");
        mSpinnerReligion.setTitle("Select Religion");
        mSpinnerMotherTongue.setTitle("Select Mother Tongue");
        mRelationshipArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerRelationship.setAdapter(mRelationshipArrayAdapter);
        mReligionArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerReligion.setAdapter(mReligionArrayAdapter);
        mMotherTongueArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerMotherTongue.setAdapter(mMotherTongueArrayAdapter);
    }

    private void getDataSignUp() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(DATA_SIGN_UP, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayRegister();
                        clearData();
                        handleDataSignUpResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                displayOops();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "data_sign_up");
    }

    private void clearData() {
        mRelationshipList.clear();
        mReligionList.clear();
        mMotherTongueList.clear();
    }

    private void handleDataSignUpResponse(JSONObject response) {
        try {
            JSONArray relationshipArray = response.getJSONArray(KEY_RELATIONSHIP);
            for (int i = 0; i < relationshipArray.length(); i++) {
                JSONObject relationshipObject = relationshipArray.getJSONObject(i);
                int id = relationshipObject.getInt(KEY_ID);
                String relation = relationshipObject.getString(KEY_NAME);
                mRelationshipList.add(new Relationship(id, relation));
            }
            mRelationshipArrayAdapter.notifyDataSetChanged();

            JSONArray religionArray = response.getJSONArray(KEY_RELIGION);
            for (int i = 0; i < religionArray.length(); i++) {
                JSONObject religionObject = religionArray.getJSONObject(i);
                int id = religionObject.getInt(KEY_ID);
                String religion = religionObject.getString(KEY_NAME);
                mReligionList.add(new Religion(id, religion));
            }
            mReligionArrayAdapter.notifyDataSetChanged();

            JSONArray motherTongueArray = response.getJSONArray(KEY_MOTHER_TONGUE);
            for (int i = 0; i < motherTongueArray.length(); i++) {
                JSONObject motherTongueObject = motherTongueArray.getJSONObject(i);
                int id = motherTongueObject.getInt(KEY_ID);
                String motherTongue = motherTongueObject.getString(KEY_NAME);
                mMotherTongueList.add(new MotherTongue(id, motherTongue));
            }
            mMotherTongueArrayAdapter.notifyDataSetChanged();

            mCountryCode = response.getString(KEY_COUNTRY_CODE);
            processCountryCode();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getGender(String relationship) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                GENDER_API_URL + relationship + "&key=" + GENDER_API_KEY, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        handleGenderResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "gender");
    }

    private void handleGenderResponse(JSONObject response) {
        try {
            String gender = response.getString(KEY_GENDER);
            if (gender.equalsIgnoreCase("male")) {
                mButtonMale.setChecked(true);
            } else if (gender.equalsIgnoreCase("female")) {
                mButtonFemale.setChecked(true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void promptDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR) - 18;
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        openDatePickerDialog(year, month, dayOfMonth);
    }

    private void openDatePickerDialog(int year, int month, int dayOfMonth) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        mEditTextDob.setText(getDate(year, month + 1, dayOfMonth));
                    }
                }, year, month, dayOfMonth);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private String getDate(int year, int month, int dayOfMonth) {
        return year + "-" + (month < 10 ? "0" + month : month) + "-" + (dayOfMonth < 10 ? "0"
                + dayOfMonth : dayOfMonth);
    }

    private void processCountryCode() {
        Country country = Country.getCountryByISO(mCountryCode);
        mImageViewCountry.setImageResource(country.getFlag());
        mTextViewDialCode.setText(country.getDialCode());
    }

    private String getDialCode() {
        Country country = Country.getCountryByISO(mCountryCode);
        return country.getDialCode();
    }

    private void displayCountryPicker() {
        final CountryPicker picker = CountryPicker.newInstance("Select Country");
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode,
                    int flagDrawableResID) {
                mCountryCode = code;
                processCountryCode();
                picker.dismiss();
            }
        });
        picker.show(getChildFragmentManager(), "COUNTRY_PICKER");
    }

    private void processRegister() {
        String firstName = mEditTextFirstName.getText().toString().trim();
        String lastName = mEditTextLastName.getText().toString().trim();
        String dob = mEditTextDob.getText().toString().trim();
        String mobile = mEditTextMobile.getText().toString().trim();
        String email = mEditTextEmail.getText().toString().trim();
        String password = mEditTextPass.getText().toString().trim();
        if (validateInput(firstName, lastName, dob, mobile, email, password)) {
            showProgressDialog("Registering..");
            register(getRegisterJsonRequest(firstName, lastName, dob, mobile, email, password));
        }
    }

    private boolean validateInput(String firstName, String lastName, String dob, String mobile,
            String email, String password) {
        if (firstName.isEmpty()) {
            mEditTextFirstName.requestFocus();
            mEditTextFirstName.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "First Name"));
            return false;
        } else if (lastName.isEmpty()) {
            mEditTextLastName.requestFocus();
            mEditTextLastName.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Last Name"));
            return false;
        } else if (dob.isEmpty()) {
            ToastBuilder.build(mContext,
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Date of Birth"));
            return false;
        } else if (mobile.isEmpty()) {
            mEditTextMobile.requestFocus();
            mEditTextMobile.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Mobile Number"));
            return false;
        } else if (email.isEmpty()) {
            mEditTextEmail.requestFocus();
            mEditTextEmail.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty), "Email"));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEditTextEmail.requestFocus();
            mEditTextEmail.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_invalid), "Email"));
            return false;
        } else if (password.isEmpty()) {
            mEditTextPass.requestFocus();
            mEditTextPass.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Password"));
            return false;
        } else if (password.length() < 6) {
            mEditTextPass.requestFocus();
            mEditTextPass.setError(getString(R.string.error_pass_length));
            return false;
        }
        return true;
    }

    private JSONObject getRegisterJsonRequest(String firstName, String lastName, String dob,
            String mobile, String email, String password) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_FIRST_NAME, firstName);
            jsonObject.put(KEY_LAST_NAME, lastName);
            jsonObject.put(KEY_USERNAME, mobile);
            jsonObject.put(KEY_EMAIL, email);
            jsonObject.put(KEY_PASSWORD, password);
            JSONObject userDetailsObject = new JSONObject();
            userDetailsObject.put(KEY_RELATIONSHIP,
                    mRelationshipList.get(mSpinnerRelationship.getSelectedItemPosition()).getId());
            userDetailsObject.put(KEY_COUNTRY_CODE, getDialCode());
            userDetailsObject.put(KEY_GENDER,
                    mGroupGender.getCheckedRadioButtonId() == R.id.radio_male ? getString(
                            R.string.radio_male) : getString(R.string.radio_female));
            userDetailsObject.put(KEY_DOB, dob);
            userDetailsObject.put(KEY_MOTHER_TONGUE,
                    mMotherTongueList.get(mSpinnerMotherTongue.getSelectedItemPosition()).getId());
            userDetailsObject.put(KEY_RELIGION,
                    mReligionList.get(mSpinnerReligion.getSelectedItemPosition()).getId());
            jsonObject.put(KEY_USERDETAILS, userDetailsObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void register(JSONObject registerJsonRequest) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(REGISTER, registerJsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgressDialog();
                        mPreference.setToken("c1a8b9997b91ca1a085067eddd8663041aa5e181");
                        mCallback.launchNextScreen();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                mPreference.setToken("c1a8b9997b91ca1a085067eddd8663041aa5e181");
                mCallback.launchNextScreen();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "register");
    }

    private void displayRegister() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutRegister.setVisibility(View.VISIBLE);
        mButtonContinue.setVisibility(View.VISIBLE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
        mLayoutRegister.setVisibility(View.GONE);
        mButtonContinue.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayOops() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutRegister.setVisibility(View.GONE);
        mButtonContinue.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.VISIBLE);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
