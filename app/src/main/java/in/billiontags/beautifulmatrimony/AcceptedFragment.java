package in.billiontags.beautifulmatrimony;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.adapter.MatrimonyAdapter;
import in.billiontags.beautifulmatrimony.model.ProfileItem;
import link.fls.swipestack.SwipeStack;


/**
 * A simple {@link Fragment} subclass.
 */
public class AcceptedFragment extends Fragment implements SwipeStack.SwipeStackListener {

    private Context mContext;
    private View mRootView;
    private SwipeStack mSwipeStack;
    private List<ProfileItem> mProfileItemList;
    private MatrimonyAdapter mAdapter;

    public AcceptedFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_accepted, container, false);
        initObjects();
        initCallbacks();
        initAdapterView();
        populateData();
        return mRootView;
    }

    @Override
    public void onViewSwipedToLeft(int position) {
        Toast.makeText(mContext, "Hate ya", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onViewSwipedToRight(int position) {
        Toast.makeText(mContext, "Love ya", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStackEmpty() {
        populateData();
    }

    private void initObjects() {
        mSwipeStack = (SwipeStack) mRootView.findViewById(R.id.profile);

        mContext = getActivity();
        mProfileItemList = new ArrayList<>();
//        mAdapter = new MatrimonyAdapter(mContext, mProfileItemList);
    }

    private void initCallbacks() {
        mSwipeStack.setListener(this);
    }

    private void initAdapterView() {
        mSwipeStack.setAdapter(mAdapter);
    }

    private void populateData() {
        mProfileItemList.add(
                new ProfileItem(3, 95, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                        "http://i.imgur.com/xCM0CKx.jpg"));
        mProfileItemList.add(
                new ProfileItem(4, 96, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                        "http://i.imgur.com/s5fGQkY.jpg"));
        mProfileItemList.add(
                new ProfileItem(5, 97, "xxx xxx, 29 years old", "Saidapet, Tamil Nadu",
                        "http://i.imgur.com/TBhAZ3U.jpg"));
        mProfileItemList.add(
                new ProfileItem(1, 93, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                        "http://i.imgur.com/7DtMVjU.jpg"));
        mProfileItemList.add(
                new ProfileItem(2, 94, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                        "http://i.imgur.com/CEGlxcp.jpg"));
        mAdapter.notifyDataSetChanged();
    }
}
