package in.billiontags.beautifulmatrimony;

import static in.billiontags.beautifulmatrimony.app.Activity.launch;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.adapter.ChatAdapter;
import in.billiontags.beautifulmatrimony.model.ChatItem;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChatActivity extends AppCompatActivity implements ChatAdapter.ChatClickListener {

    private Context mContext;
    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private List<Object> mObjectList;
    private LinearLayoutManager mLayoutManager;
    private ChatAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        initObjects();
        initToolbar();
        initRecyclerView();
        populateData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onChatClick(int id) {
        launch(mContext, ConversationActivity.class);
    }

    private void initObjects() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRecyclerView = (RecyclerView) findViewById(R.id.chat_list);

        mContext = this;
        mObjectList = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(mContext);
        mAdapter = new ChatAdapter(mContext, mObjectList, this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
        }
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(
                new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void populateData() {
        mObjectList.add(new ChatItem(2, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/CEGlxcp.jpg"));
        mObjectList.add(new ChatItem(3, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/xCM0CKx.jpg"));
        mObjectList.add(new ChatItem(1, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/7DtMVjU.jpg"));
        mObjectList.add(new ChatItem(5, "xxx xxx, 29 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/TBhAZ3U.jpg"));
        mObjectList.add(new ChatItem(1, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/7DtMVjU.jpg"));
        mObjectList.add(new ChatItem(2, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/CEGlxcp.jpg"));
        mObjectList.add(new ChatItem(3, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/xCM0CKx.jpg"));
        mObjectList.add(new ChatItem(4, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/s5fGQkY.jpg"));
        mObjectList.add(new ChatItem(4, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/s5fGQkY.jpg"));
        mObjectList.add(new ChatItem(5, "xxx xxx, 29 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/TBhAZ3U.jpg"));
        mObjectList.add(new ChatItem(1, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/7DtMVjU.jpg"));
        mObjectList.add(new ChatItem(2, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/CEGlxcp.jpg"));
        mObjectList.add(new ChatItem(3, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/xCM0CKx.jpg"));
        mObjectList.add(new ChatItem(4, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/s5fGQkY.jpg"));
        mObjectList.add(new ChatItem(3, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/xCM0CKx.jpg"));
        mObjectList.add(new ChatItem(4, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/s5fGQkY.jpg"));
        mObjectList.add(new ChatItem(5, "xxx xxx, 29 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/TBhAZ3U.jpg"));
        mObjectList.add(new ChatItem(1, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/7DtMVjU.jpg"));
        mObjectList.add(new ChatItem(2, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/CEGlxcp.jpg"));
        mObjectList.add(new ChatItem(5, "xxx xxx, 29 years old", "Saidapet, Tamil Nadu",
                "http://i.imgur.com/TBhAZ3U.jpg"));
        mAdapter.notifyDataSetChanged();
    }
}
