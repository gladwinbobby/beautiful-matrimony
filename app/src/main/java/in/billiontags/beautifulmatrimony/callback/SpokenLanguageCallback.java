package in.billiontags.beautifulmatrimony.callback;

/**
 * Created by Bobby on 06/07/17
 */

public interface SpokenLanguageCallback {
    void onSpokenLanguageClick(int position);
}
