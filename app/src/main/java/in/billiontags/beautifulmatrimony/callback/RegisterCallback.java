package in.billiontags.beautifulmatrimony.callback;

/**
 * Created by Bobby on 07/07/17
 */

public interface RegisterCallback {
    void launchNextScreen();
}
