package in.billiontags.beautifulmatrimony.callback;

/**
 * Created by Bobby on 06/07/17
 */

public interface FavoriteMusicCallback {
    void onFavoriteMusicClick(int position);
}
