package in.billiontags.beautifulmatrimony;


import static in.billiontags.beautifulmatrimony.app.Api.KEY_AREA;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_CITY;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_COUNT;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_COUNTRY;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_FACEBOOK_PROFILE_PIC;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_FIRST_NAME;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_GENDER;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_GOOGLE_PROFILE_PIC;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_ID;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_IS_IGNORED;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_IS_SHORTLISTED;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_LAST_NAME;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_NAME;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_PROFILE_PIC;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_RESULTS;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_STATE;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_STREET;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_UID;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_USERDETAILS;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_USERPROFILE;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_USER_ADDRESS;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_ZIPCODE;
import static in.billiontags.beautifulmatrimony.app.Api.MATRIMONY;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.billiontags.beautifulmatrimony.adapter.MatrimonyAdapter;
import in.billiontags.beautifulmatrimony.app.AppController;
import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.app.VolleyErrorHandler;
import in.billiontags.beautifulmatrimony.model.Matrimony;
import link.fls.swipestack.SwipeStack;


/**
 * A simple {@link Fragment} subclass.
 */
public class MatrimonyFragment extends Fragment implements TabLayout.OnTabSelectedListener,
        SwipeStack.SwipeStackListener {

    private Context mContext;
    private View mRootView;
    private TabLayout mTabLayout;
    private SwipeStack mSwipeStack;
    private FrameLayout mLayoutLoading;
    private LinearLayout mLayoutEmpty, mLayoutOops;
    private List<Matrimony> mMatrimonyList;
    private MatrimonyAdapter mAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private String mUrl;
    private int mType = 1;

    public MatrimonyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_matrimony, container, false);
        initObjects();
        initCallbacks();
        initTabs();
        initAdapterView();
        setTabsFont();
        return mRootView;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mType = tab.getPosition() + 1;
        setUrl();
        displayLoading();
        getMatrimony();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onViewSwipedToLeft(int position) {
        Toast.makeText(mContext, "Hate ya", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onViewSwipedToRight(int position) {
        Toast.makeText(mContext, "Love ya", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStackEmpty() {
        if (mUrl != null) {
            displayLoading();
            getMatrimony();
        } else {
            displayEmpty();
        }
    }

    private void initObjects() {
        mTabLayout = (TabLayout) mRootView.findViewById(R.id.tab);
        mSwipeStack = (SwipeStack) mRootView.findViewById(R.id.profile);
        mLayoutLoading = (FrameLayout) mRootView.findViewById(R.id.loading);
        mLayoutEmpty = (LinearLayout) mRootView.findViewById(R.id.empty);
        mLayoutOops = (LinearLayout) mRootView.findViewById(R.id.oops);

        mContext = getActivity();
        mMatrimonyList = new ArrayList<>();
        mAdapter = new MatrimonyAdapter(mContext, mMatrimonyList);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mTabLayout.addOnTabSelectedListener(this);
        mSwipeStack.setListener(this);
    }

    private void initTabs() {
        mTabLayout.addTab(mTabLayout.newTab().setText("Who Viewed Your Profile"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Who Shortlisted Me"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Mobile Numbers Viewed By Me"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Who Viewed My Contact No"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Profiles I Have Ignored"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Profiles I Have Blocked"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Profiles Shortlisted By Me"));
    }

    private void initAdapterView() {
        mSwipeStack.setAdapter(mAdapter);
    }

    private void setTabsFont() {
        ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(
                            Typeface.createFromAsset(mContext.getAssets(), "fonts/coves_light.otf"),
                            Typeface.NORMAL);
                    ((TextView) tabViewChild).setTextSize(
                            getResources().getDimension(R.dimen.small_txt));
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }

    private void setUrl() {
        mUrl = MATRIMONY + "?type=" + mType;
    }

    private void getMatrimony() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(mUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (isAdded()) {
                            displayMatrimony();
                            handleMatrimonyResponse(response);
                            isEmpty();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (isAdded()) {
                    displayOops();
                    VolleyErrorHandler.handle(mContext, error);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + mPreference.getToken());
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "matrimony");
    }

    private void handleMatrimonyResponse(JSONObject response) {

        try {
            mUrl = response.getString(KEY_COUNT);
            JSONArray resultsArray = response.getJSONArray(KEY_RESULTS);
            for (int i = 0; i < resultsArray.length(); i++) {
                JSONObject jsonObject = resultsArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String firstName = jsonObject.getString(KEY_FIRST_NAME);
                String lastName = jsonObject.getString(KEY_LAST_NAME);
                JSONObject userProfileObject = jsonObject.getJSONObject(KEY_USERPROFILE);
                int uId = userProfileObject.getInt(KEY_UID);
                String profilePic = null;
                if (!userProfileObject.isNull(KEY_PROFILE_PIC)) {
                    profilePic = userProfileObject.getString(KEY_PROFILE_PIC);
                } else if (!userProfileObject.isNull(KEY_GOOGLE_PROFILE_PIC)) {
                    profilePic = userProfileObject.getString(KEY_GOOGLE_PROFILE_PIC);
                } else if (!userProfileObject.isNull(KEY_FACEBOOK_PROFILE_PIC)) {
                    profilePic = userProfileObject.getString(KEY_FACEBOOK_PROFILE_PIC);
                }
                JSONObject userDetailsObject = jsonObject.getJSONObject(KEY_USERDETAILS);
                String gender = userDetailsObject.getString(KEY_GENDER);
                JSONObject addressObject = jsonObject.getJSONObject(KEY_USER_ADDRESS);
                String street = addressObject.getString(KEY_STREET);
                String area = addressObject.getString(KEY_AREA);
                JSONObject cityObject = addressObject.getJSONObject(KEY_CITY);
                String city = cityObject.getString(KEY_NAME);
                JSONObject stateObject = cityObject.getJSONObject(KEY_STATE);
                String state = stateObject.getString(KEY_NAME);
                JSONObject countryObject = stateObject.getJSONObject(KEY_COUNTRY);
                String country = countryObject.getString(KEY_NAME);
                String zipCode = addressObject.getString(KEY_ZIPCODE);
                boolean isShortlisted = jsonObject.getBoolean(KEY_IS_SHORTLISTED);
                boolean isIgnored = jsonObject.getBoolean(KEY_IS_IGNORED);
                mMatrimonyList.add(
                        new Matrimony(id, uId, firstName, lastName, profilePic, gender, street,
                                area, city, state, country, zipCode, isShortlisted, isIgnored));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mAdapter.notifyDataSetChanged();
    }

    private void isEmpty() {
        if (mMatrimonyList.isEmpty()) {
            displayEmpty();
        }
    }

    private void displayMatrimony() {
        mLayoutLoading.setVisibility(View.GONE);
        mSwipeStack.setVisibility(View.VISIBLE);
        mLayoutEmpty.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayLoading() {
        mLayoutLoading.setVisibility(View.VISIBLE);
        mSwipeStack.setVisibility(View.GONE);
        mLayoutEmpty.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayEmpty() {
        mLayoutLoading.setVisibility(View.GONE);
        mSwipeStack.setVisibility(View.GONE);
        mLayoutEmpty.setVisibility(View.VISIBLE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayOops() {
        mLayoutLoading.setVisibility(View.GONE);
        mSwipeStack.setVisibility(View.GONE);
        mLayoutEmpty.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.VISIBLE);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
