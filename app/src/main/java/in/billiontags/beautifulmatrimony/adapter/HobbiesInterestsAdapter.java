package in.billiontags.beautifulmatrimony.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.billiontags.beautifulmatrimony.R;
import in.billiontags.beautifulmatrimony.callback.HobbiesInterestsCallback;
import in.billiontags.beautifulmatrimony.holder.HobbiesInterestsHolder;
import in.billiontags.beautifulmatrimony.model.Hobby;

/**
 * Created by Bobby on 06/07/17
 */

public class HobbiesInterestsAdapter extends RecyclerView.Adapter<HobbiesInterestsHolder> {

    private List<Hobby> mHobbyList;
    private HobbiesInterestsCallback mCallback;

    public HobbiesInterestsAdapter(List<Hobby> hobbyList, HobbiesInterestsCallback callback) {
        mHobbyList = hobbyList;
        mCallback = callback;
    }

    @Override
    public HobbiesInterestsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HobbiesInterestsHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checkbox, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(HobbiesInterestsHolder holder, int position) {
        Hobby hobby = mHobbyList.get(position);
        holder.mTextViewData.setText(hobby.getHobby());
        holder.mTextViewData.setChecked(hobby.isSelected());
    }

    @Override
    public int getItemCount() {
        return mHobbyList.size();
    }
}
