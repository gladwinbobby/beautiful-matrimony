package in.billiontags.beautifulmatrimony.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.billiontags.beautifulmatrimony.R;
import in.billiontags.beautifulmatrimony.callback.FavoriteMusicCallback;
import in.billiontags.beautifulmatrimony.holder.FavoriteMusicHolder;
import in.billiontags.beautifulmatrimony.model.Music;

/**
 * Created by Bobby on 06/07/17
 */

public class FavoriteMusicAdapter extends RecyclerView.Adapter<FavoriteMusicHolder> {

    private List<Music> mMusicList;
    private FavoriteMusicCallback mCallback;

    public FavoriteMusicAdapter(List<Music> musicList, FavoriteMusicCallback callback) {
        mMusicList = musicList;
        mCallback = callback;
    }

    @Override
    public FavoriteMusicHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FavoriteMusicHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checkbox, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(FavoriteMusicHolder holder, int position) {
        Music music = mMusicList.get(position);
        holder.mTextViewData.setText(music.getMusic());
        holder.mTextViewData.setChecked(music.isSelected());
    }

    @Override
    public int getItemCount() {
        return mMusicList.size();
    }
}
