package in.billiontags.beautifulmatrimony.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.billiontags.beautifulmatrimony.R;
import in.billiontags.beautifulmatrimony.callback.SpokenLanguageCallback;
import in.billiontags.beautifulmatrimony.holder.SpokenLanguageHolder;
import in.billiontags.beautifulmatrimony.model.Language;

/**
 * Created by Bobby on 06/07/17
 */

public class SpokenLanguageAdapter extends RecyclerView.Adapter<SpokenLanguageHolder> {

    private List<Language> mLanguageList;
    private SpokenLanguageCallback mCallback;

    public SpokenLanguageAdapter(List<Language> languageList, SpokenLanguageCallback callback) {
        mLanguageList = languageList;
        mCallback = callback;
    }

    @Override
    public SpokenLanguageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SpokenLanguageHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checkbox, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(SpokenLanguageHolder holder, int position) {
        Language language = mLanguageList.get(position);
        holder.mTextViewData.setText(language.getLanguage());
        holder.mTextViewData.setChecked(language.isSelected());
    }

    @Override
    public int getItemCount() {
        return mLanguageList.size();
    }
}
