package in.billiontags.beautifulmatrimony.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import in.billiontags.beautifulmatrimony.R;
import in.billiontags.beautifulmatrimony.holder.ProgressHolder;
import in.billiontags.beautifulmatrimony.holder.SuggestionHolder;
import in.billiontags.beautifulmatrimony.model.SuggestionItem;

/**
 * Created by Bobby on 02-05-2017
 */

public class SuggestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_NONE = -1;
    private static final int TYPE_PROGRESS = 0;
    private static final int TYPE_SUGGESTION = 1;
    private Context mContext;
    private List<Object> mObjectList;
    private SuggestionClickListener mListener;

    public SuggestionAdapter(Context context, List<Object> objectList,
            SuggestionClickListener listener) {
        mContext = context;
        mObjectList = objectList;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_PROGRESS:
                return new ProgressHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress,
                                parent, false));
            case TYPE_SUGGESTION:
                return new SuggestionHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_suggestion,
                                parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case TYPE_PROGRESS:
                bindProgressViewHolder((ProgressHolder) holder);
                break;
            case TYPE_SUGGESTION:
                bindSuggestionViewHolder((SuggestionHolder) holder, position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mObjectList.get(position) == null) {
            return TYPE_PROGRESS;
        } else if (mObjectList.get(position) instanceof SuggestionItem) {
            return TYPE_SUGGESTION;
        } else {
            return TYPE_NONE;
        }
    }

    @Override
    public int getItemCount() {
        return mObjectList.size();
    }

    private void bindProgressViewHolder(ProgressHolder holder) {
        holder.getProgressBar().setIndeterminate(true);
    }

    private void bindSuggestionViewHolder(SuggestionHolder holder, int position) {
        SuggestionItem suggestionItem = (SuggestionItem) mObjectList.get(position);
        Glide.with(mContext).load(suggestionItem.getImage()).into(holder.getImageViewProfile());
        holder.getTextViewIdentity().setText(suggestionItem.getIdentity());
    }

    public interface SuggestionClickListener {
        void onSuggestionClick(int id);
    }
}
