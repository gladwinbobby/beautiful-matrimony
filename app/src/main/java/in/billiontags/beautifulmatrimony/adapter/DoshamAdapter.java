package in.billiontags.beautifulmatrimony.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.billiontags.beautifulmatrimony.R;
import in.billiontags.beautifulmatrimony.callback.DoshamCallback;
import in.billiontags.beautifulmatrimony.holder.DoshamHolder;
import in.billiontags.beautifulmatrimony.model.Dosham;

/**
 * Created by Bobby on 04/07/17
 */

public class DoshamAdapter extends RecyclerView.Adapter<DoshamHolder> {

    private List<Dosham> mDoshamList;
    private DoshamCallback mCallback;

    public DoshamAdapter(List<Dosham> doshamList, DoshamCallback callback) {
        mDoshamList = doshamList;
        mCallback = callback;
    }

    @Override
    public DoshamHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DoshamHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checkbox, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(DoshamHolder holder, int position) {
        Dosham dosham = mDoshamList.get(position);
        holder.mTextViewData.setText(dosham.getDosham());
        holder.mTextViewData.setChecked(dosham.isSelected());
    }

    @Override
    public int getItemCount() {
        return mDoshamList.size();
    }
}
