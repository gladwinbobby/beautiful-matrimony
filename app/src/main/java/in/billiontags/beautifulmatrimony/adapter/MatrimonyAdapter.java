package in.billiontags.beautifulmatrimony.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Locale;

import in.billiontags.beautifulmatrimony.R;
import in.billiontags.beautifulmatrimony.model.Matrimony;

/**
 * Created by Bobby on 02-05-2017
 */

public class MatrimonyAdapter extends BaseAdapter {

    private Context mContext;
    private List<Matrimony> mMatrimonyList;

    public MatrimonyAdapter(Context context, List<Matrimony> matrimonyList) {
        mContext = context;
        mMatrimonyList = matrimonyList;
    }

    @Override
    public int getCount() {
        return mMatrimonyList.size();
    }

    @Override
    public Object getItem(int position) {
        return mMatrimonyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_matrimony,
                    parent, false);
            holder = new ViewHolder();
            holder.mImageViewProfile = (ImageView) convertView.findViewById(R.id.img_profile);
            holder.mTextViewIdentity = (TextView) convertView.findViewById(R.id.txt_identity);
            holder.mTextViewLocation = (TextView) convertView.findViewById(R.id.txt_location);
            holder.mTextViewMatch = (TextView) convertView.findViewById(R.id.txt_match);
            holder.mTextViewViewProfile = (TextView) convertView.findViewById(
                    R.id.txt_view_profile);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Matrimony matrimony = mMatrimonyList.get(position);
        Glide.with(mContext).load(matrimony.getProfilePic()).into(holder.mImageViewProfile);
        holder.mTextViewIdentity.setText(matrimony.getFirstName() + " " + matrimony.getLastName());
        holder.mTextViewLocation.setText(matrimony.getCity() + ", " + matrimony.getState());
        holder.mTextViewMatch.setText(
                String.format(Locale.getDefault(), mContext.getString(R.string.format_percent),
                        96));
        return convertView;
    }

    private static class ViewHolder {
        private ImageView mImageViewProfile;
        private TextView mTextViewIdentity, mTextViewLocation, mTextViewMatch, mTextViewViewProfile;
    }
}
