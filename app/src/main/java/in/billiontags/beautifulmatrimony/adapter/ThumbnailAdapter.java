package in.billiontags.beautifulmatrimony.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import in.billiontags.beautifulmatrimony.R;
import in.billiontags.beautifulmatrimony.holder.ThumbnailHolder;
import in.billiontags.beautifulmatrimony.model.ThumbnailItem;

/**
 * Created by Bobby on 03-05-2017
 */

public class ThumbnailAdapter extends RecyclerView.Adapter<ThumbnailHolder> {

    private Context mContext;
    private List<ThumbnailItem> mThumbnailItemList;
    private ThumbnailClickListener mListener;

    public ThumbnailAdapter(Context context, List<ThumbnailItem> thumbnailItemList,
            ThumbnailClickListener listener) {
        mContext = context;
        mThumbnailItemList = thumbnailItemList;
        mListener = listener;
    }

    @Override
    public ThumbnailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ThumbnailHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_thumbnail, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(final ThumbnailHolder holder, int position) {
        ThumbnailItem thumbnailItem = mThumbnailItemList.get(position);
        Glide.with(mContext).load(thumbnailItem.getThumbnail()).into(
                holder.getImageViewThumbnail());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onThumbnailClick(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mThumbnailItemList.size();
    }

    public interface ThumbnailClickListener {
        void onThumbnailClick(int position);
    }
}
