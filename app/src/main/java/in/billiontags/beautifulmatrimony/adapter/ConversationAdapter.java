package in.billiontags.beautifulmatrimony.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import in.billiontags.beautifulmatrimony.R;
import in.billiontags.beautifulmatrimony.holder.MessageHolder;
import in.billiontags.beautifulmatrimony.holder.ResponseHolder;
import in.billiontags.beautifulmatrimony.model.MessageItem;
import in.billiontags.beautifulmatrimony.model.ResponseItem;

/**
 * Created by Bobby on 03-05-2017
 */

public class ConversationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_NONE = -1;
    private static final int TYPE_MESSAGE = 1;
    private static final int TYPE_RESPONSE = 2;
    private Context mContext;
    private List<Object> mObjectList;

    public ConversationAdapter(Context context, List<Object> objectList) {
        mContext = context;
        mObjectList = objectList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_MESSAGE:
                return new MessageHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message,
                                parent, false));
            case TYPE_RESPONSE:
                return new ResponseHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_response,
                                parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case TYPE_MESSAGE:
                bindMessageViewHolder((MessageHolder) holder, position);
                break;
            case TYPE_RESPONSE:
                bindResponseViewHolder((ResponseHolder) holder, position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mObjectList.get(position) instanceof MessageItem) {
            return TYPE_MESSAGE;
        } else if (mObjectList.get(position) instanceof ResponseItem) {
            return TYPE_RESPONSE;
        } else {
            return TYPE_NONE;
        }
    }

    @Override
    public int getItemCount() {
        return mObjectList.size();
    }

    private void bindMessageViewHolder(MessageHolder holder, int position) {
        MessageItem messageItem = (MessageItem) mObjectList.get(position);
        Glide.with(mContext).load(messageItem.getImage()).into(holder.getImageViewProfile());
        holder.getTextViewMessage().setText(messageItem.getMessage());
    }

    private void bindResponseViewHolder(ResponseHolder holder, int position) {
        ResponseItem responseItem = (ResponseItem) mObjectList.get(position);
        holder.getTextViewMessage().setText(responseItem.getMessage());
    }
}
