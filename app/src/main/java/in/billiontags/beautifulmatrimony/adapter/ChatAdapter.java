package in.billiontags.beautifulmatrimony.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import in.billiontags.beautifulmatrimony.R;
import in.billiontags.beautifulmatrimony.holder.ChatHolder;
import in.billiontags.beautifulmatrimony.holder.ProgressHolder;
import in.billiontags.beautifulmatrimony.model.ChatItem;

/**
 * Created by Bobby on 02-05-2017
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_NONE = -1;
    private static final int TYPE_PROGRESS = 0;
    private static final int TYPE_CHAT = 1;
    private Context mContext;
    private List<Object> mObjectList;
    private ChatClickListener mListener;

    public ChatAdapter(Context context, List<Object> objectList, ChatClickListener listener) {
        mContext = context;
        mObjectList = objectList;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_PROGRESS:
                return new ProgressHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress,
                                parent, false));
            case TYPE_CHAT:
                return new ChatHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent,
                                false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case TYPE_PROGRESS:
                bindProgressViewHolder((ProgressHolder) holder);
                break;
            case TYPE_CHAT:
                bindChatViewHolder((ChatHolder) holder, position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mObjectList.get(position) == null) {
            return TYPE_PROGRESS;
        } else if (mObjectList.get(position) instanceof ChatItem) {
            return TYPE_CHAT;
        } else {
            return TYPE_NONE;
        }
    }

    @Override
    public int getItemCount() {
        return mObjectList.size();
    }

    private void bindProgressViewHolder(ProgressHolder holder) {
        holder.getProgressBar().setIndeterminate(true);
    }

    private void bindChatViewHolder(ChatHolder holder, int position) {
        final ChatItem chatItem = (ChatItem) mObjectList.get(position);
        Glide.with(mContext).load(chatItem.getImage()).into(holder.getImageViewProfile());
        holder.getTextViewIdentity().setText(chatItem.getIdentity());
        holder.getTextViewLocation().setText(chatItem.getLocation());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onChatClick(chatItem.getId());
            }
        });
    }

    public interface ChatClickListener {
        void onChatClick(int id);
    }
}
