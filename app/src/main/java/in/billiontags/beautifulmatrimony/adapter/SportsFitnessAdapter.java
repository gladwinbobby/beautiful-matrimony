package in.billiontags.beautifulmatrimony.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.billiontags.beautifulmatrimony.R;
import in.billiontags.beautifulmatrimony.callback.SportsFitnessCallback;
import in.billiontags.beautifulmatrimony.holder.SportsFitnessHolder;
import in.billiontags.beautifulmatrimony.model.Sport;

/**
 * Created by Bobby on 06/07/17
 */

public class SportsFitnessAdapter extends RecyclerView.Adapter<SportsFitnessHolder> {

    private List<Sport> mSportList;
    private SportsFitnessCallback mCallback;

    public SportsFitnessAdapter(List<Sport> sportList, SportsFitnessCallback callback) {
        mSportList = sportList;
        mCallback = callback;
    }

    @Override
    public SportsFitnessHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SportsFitnessHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checkbox, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(SportsFitnessHolder holder, int position) {
        Sport sport = mSportList.get(position);
        holder.mTextViewData.setText(sport.getSport());
        holder.mTextViewData.setChecked(sport.isSelected());
    }

    @Override
    public int getItemCount() {
        return mSportList.size();
    }
}
