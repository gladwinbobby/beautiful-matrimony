package in.billiontags.beautifulmatrimony.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.model.Age;
import in.billiontags.beautifulmatrimony.model.BodyType;
import in.billiontags.beautifulmatrimony.model.Employment;
import in.billiontags.beautifulmatrimony.model.FamilyStatus;
import in.billiontags.beautifulmatrimony.model.FamilyValue;
import in.billiontags.beautifulmatrimony.model.FatherStatus;
import in.billiontags.beautifulmatrimony.model.HeightCm;
import in.billiontags.beautifulmatrimony.model.HeightFt;
import in.billiontags.beautifulmatrimony.model.Income;
import in.billiontags.beautifulmatrimony.model.MaritalStatus;
import in.billiontags.beautifulmatrimony.model.MotherStatus;
import in.billiontags.beautifulmatrimony.model.SkinToneType;
import in.billiontags.beautifulmatrimony.model.Weight;

/**
 * Created by Bobby on 05/07/17
 */

public class Utils {
    public static List<MaritalStatus> getMaritalStatusList() {
        List<MaritalStatus> maritalStatusList = new ArrayList<>();
        maritalStatusList.add(new MaritalStatus(1, "Never Married"));
        maritalStatusList.add(new MaritalStatus(2, "Widowed"));
        maritalStatusList.add(new MaritalStatus(3, "Divorced"));
        maritalStatusList.add(new MaritalStatus(4, "Awaiting Divorce"));
        return maritalStatusList;
    }

    public static List<HeightFt> getHeightFtList() {
        List<HeightFt> heightFtList = new ArrayList<>();
        for (float i = 1.07f; i <= 8.00f; i += 0.01f) {
            String[] height = String.valueOf(new DecimalFormat("#.00").format(i)).split("\\.");
            int ft = Integer.parseInt(height[0]);
            int in = Integer.parseInt(height[1]);
            if (in < 12) {
                heightFtList.add(new HeightFt(i, ft + "ft " + in + "in"));
            } else {
                ft++;
                i = ft;
                heightFtList.add(new HeightFt(i, ft + "ft"));
            }
        }
        return heightFtList;
    }

    public static List<HeightCm> getHeightCmList() {
        List<HeightCm> heightCmList = new ArrayList<>();
        int lastHeight = 32;
        for (float i = 1.07f; i <= 8.00f; i += 0.01f) {
            String[] height = String.valueOf(new DecimalFormat("#.00").format(i)).split("\\.");
            int ft = Integer.parseInt(height[0]);
            int in = Integer.parseInt(height[1]);
            if (in > 11) {
                ft++;
                i = ft;
            }
            int heightCm = (int) (i * 30.48);
            while (lastHeight <= heightCm) {
                heightCmList.add(new HeightCm(i, lastHeight + "cms"));
                lastHeight++;
            }
        }
        return heightCmList;
    }

    public static List<FamilyStatus> getFamilyStatusList() {
        List<FamilyStatus> familyStatusList = new ArrayList<>();
        familyStatusList.add(new FamilyStatus(1, "Middle Class"));
        familyStatusList.add(new FamilyStatus(2, "Upper Middle Class"));
        familyStatusList.add(new FamilyStatus(3, "Rich"));
        familyStatusList.add(new FamilyStatus(4, "Affluent"));
        return familyStatusList;
    }

    public static List<FamilyValue> getFamilyValueList() {
        List<FamilyValue> familyValueList = new ArrayList<>();
        familyValueList.add(new FamilyValue(1, "Orthodox"));
        familyValueList.add(new FamilyValue(2, "Traditional"));
        familyValueList.add(new FamilyValue(3, "Moderate"));
        familyValueList.add(new FamilyValue(4, "Liberal"));
        return familyValueList;
    }

    public static List<Employment> getEmploymentList() {
        List<Employment> employmentList = new ArrayList<>();
        employmentList.add(new Employment(1, "Government"));
        employmentList.add(new Employment(2, "Private"));
        employmentList.add(new Employment(3, "Business"));
        employmentList.add(new Employment(4, "Defence"));
        employmentList.add(new Employment(5, "Self Employed"));
        employmentList.add(new Employment(6, "Not Working"));
        return employmentList;
    }

    public static List<Income> getIncomeList() {
        List<Income> incomeList = new ArrayList<>();
        incomeList.add(new Income(1, "1 lakh to 3 lakhs"));
        incomeList.add(new Income(2, "3 lakhs to 5 lakhs"));
        incomeList.add(new Income(3, "5 lakhs to 10 lakhs"));
        incomeList.add(new Income(4, "10 lakhs and more"));
        return incomeList;
    }

    public static List<BodyType> getBodyTypeList() {
        List<BodyType> bodyTypeList = new ArrayList<>();
        bodyTypeList.add(new BodyType(1, "Slim"));
        bodyTypeList.add(new BodyType(2, "Average"));
        bodyTypeList.add(new BodyType(3, "Athletic"));
        bodyTypeList.add(new BodyType(4, "Heavy"));
        bodyTypeList.add(new BodyType(5, "Toned"));
        return bodyTypeList;
    }

    public static List<SkinToneType> getSkinToneTypeList() {
        List<SkinToneType> skinToneTypeList = new ArrayList<>();
        skinToneTypeList.add(new SkinToneType(1, "Black"));
        skinToneTypeList.add(new SkinToneType(2, "Brown"));
        skinToneTypeList.add(new SkinToneType(3, "Dusky"));
        skinToneTypeList.add(new SkinToneType(4, "Fair"));
        skinToneTypeList.add(new SkinToneType(5, "White"));
        return skinToneTypeList;
    }

    public static List<Weight> getWeightList() {
        List<Weight> weightList = new ArrayList<>();
        for (int i = 30; i <= 150; i++) {
            weightList.add(new Weight(i, i + " Kg"));
        }
        return weightList;
    }

    public static List<FatherStatus> getFatherStatusList() {
        List<FatherStatus> fatherStatusList = new ArrayList<>();
        fatherStatusList.add(new FatherStatus(1, "Government"));
        fatherStatusList.add(new FatherStatus(2, "Private"));
        fatherStatusList.add(new FatherStatus(3, "Business"));
        fatherStatusList.add(new FatherStatus(4, "Defence"));
        fatherStatusList.add(new FatherStatus(5, "Self Employed"));
        fatherStatusList.add(new FatherStatus(6, "Not Working"));
        return fatherStatusList;
    }

    public static List<MotherStatus> getMotherStatusList() {
        List<MotherStatus> motherStatusList = new ArrayList<>();
        motherStatusList.add(new MotherStatus(1, "Government"));
        motherStatusList.add(new MotherStatus(2, "Private"));
        motherStatusList.add(new MotherStatus(3, "Business"));
        motherStatusList.add(new MotherStatus(4, "Defence"));
        motherStatusList.add(new MotherStatus(5, "Self Employed"));
        motherStatusList.add(new MotherStatus(6, "Not Working"));
        return motherStatusList;
    }

    public static List<Age> getAgeList() {
        List<Age> ageList = new ArrayList<>();
        for (int i = 18; i <= 70; i++) {
            ageList.add(new Age(i, i == 1 ? i + " yr" : i + " yrs"));
        }
        return ageList;
    }
}
