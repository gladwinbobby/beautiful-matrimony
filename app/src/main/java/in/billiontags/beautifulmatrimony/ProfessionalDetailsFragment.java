package in.billiontags.beautifulmatrimony;


import static in.billiontags.beautifulmatrimony.app.Api.DATA_PHASE_1;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_BRANCH;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_CURRENCY;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_EDUCATION;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_ID;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_NAME;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_OCCUPATION;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_OCCUPATION_SECTOR;
import static in.billiontags.beautifulmatrimony.helper.Utils.getEmploymentList;
import static in.billiontags.beautifulmatrimony.helper.Utils.getIncomeList;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.app.AppController;
import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.app.VolleyErrorHandler;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;
import in.billiontags.beautifulmatrimony.model.Currency;
import in.billiontags.beautifulmatrimony.model.Education;
import in.billiontags.beautifulmatrimony.model.Employment;
import in.billiontags.beautifulmatrimony.model.Income;
import in.billiontags.beautifulmatrimony.model.Occupation;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfessionalDetailsFragment extends Fragment implements
        AdapterView.OnItemSelectedListener, View.OnClickListener {

    private View mRootView;
    private Context mContext;
    private ProgressBar mProgressBar;
    private SearchableSpinner mSpinnerHighestEducation, mSpinnerEmployedIn, mSpinnerOccupation,
            mSpinnerCurrency, mSpinnerIncome;
    private Button mButtonSkip, mButtonContinue, mButtonRetry;
    private LinearLayout mLayoutProfessionalDetails, mLayoutFooter, mLayoutOops;
    private List<Education> mEducationList;
    private List<Employment> mEmploymentList;
    private List<Occupation> mOccupationList;
    private List<Currency> mCurrencyList;
    private List<Income> mIncomeList;
    private ArrayAdapter<Education> mEducationArrayAdapter;
    private ArrayAdapter<Employment> mEmploymentArrayAdapter;
    private ArrayAdapter<Occupation> mOccupationArrayAdapter;
    private ArrayAdapter<Currency> mCurrencyArrayAdapter;
    private ArrayAdapter<Income> mIncomeArrayAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;

    public ProfessionalDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_professional_details, container, false);
        initObjects();
        initCallbacks();
        initSpinners();
        displayLoading();
        getData();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSkip) {
            processProfessionalDetails();
        } else if (v == mButtonContinue) {
            processProfessionalDetails();
        } else if (v == mButtonRetry) {
            displayLoading();
            getData();
        }
    }

    private void initObjects() {
        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progress);
        mSpinnerHighestEducation = (SearchableSpinner) mRootView.findViewById(
                R.id.spin_highest_education);
        mSpinnerEmployedIn = (SearchableSpinner) mRootView.findViewById(R.id.spin_employed_in);
        mSpinnerOccupation = (SearchableSpinner) mRootView.findViewById(R.id.spin_occupation);
        mSpinnerCurrency = (SearchableSpinner) mRootView.findViewById(R.id.spin_currency);
        mSpinnerIncome = (SearchableSpinner) mRootView.findViewById(R.id.spin_income);
        mButtonSkip = (Button) mRootView.findViewById(R.id.btn_skip);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);
        mButtonRetry = (Button) mRootView.findViewById(R.id.btn_retry);
        mLayoutFooter = (LinearLayout) mRootView.findViewById(R.id.footer);
        mLayoutProfessionalDetails = (LinearLayout) mRootView.findViewById(
                R.id.professional_details);
        mLayoutOops = (LinearLayout) mRootView.findViewById(R.id.oops);

        mContext = getActivity();
        mEducationList = new ArrayList<>();
        mEmploymentList = new ArrayList<>();
        mOccupationList = new ArrayList<>();
        mCurrencyList = new ArrayList<>();
        mIncomeList = new ArrayList<>();
        mEducationArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mEducationList);
        mEmploymentArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mEmploymentList);
        mOccupationArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mOccupationList);
        mCurrencyArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mCurrencyList);
        mIncomeArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mIncomeList);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonSkip.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
        mButtonRetry.setOnClickListener(this);
    }

    private void initSpinners() {
        mSpinnerHighestEducation.setTitle("Select Highest Education");
        mSpinnerEmployedIn.setTitle("Select Employment");
        mSpinnerOccupation.setTitle("Select Occupation");
        mSpinnerCurrency.setTitle("Select Currency");
        mSpinnerIncome.setTitle("Select Income");

        mEmploymentList.addAll(getEmploymentList());
        mIncomeList.addAll(getIncomeList());

        mEducationArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerHighestEducation.setAdapter(mEducationArrayAdapter);
        mEmploymentArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerEmployedIn.setAdapter(mEmploymentArrayAdapter);
        mOccupationArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerOccupation.setAdapter(mOccupationArrayAdapter);
        mCurrencyArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerCurrency.setAdapter(mCurrencyArrayAdapter);
        mIncomeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerIncome.setAdapter(mIncomeArrayAdapter);
    }

    private void clearData() {
        mEducationList.clear();
        mOccupationList.clear();
        mCurrencyList.clear();
    }

    private void getData() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(DATA_PHASE_1, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayProfessionalDetails();
                        clearData();
                        handleDataResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                displayOops();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "data_phase1");
    }

    private void handleDataResponse(JSONObject response) {
        try {
            JSONArray jsonArray = response.getJSONArray(KEY_CURRENCY);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String currency = jsonObject.getString(KEY_NAME);
                mCurrencyList.add(new Currency(id, currency));
            }
            mCurrencyArrayAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = response.getJSONArray(KEY_EDUCATION);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String education = jsonObject.getString(KEY_NAME);
                JSONArray branchArray = jsonObject.getJSONArray(KEY_BRANCH);
                for (int j = 0; j < branchArray.length(); j++) {
                    JSONObject branchObject = branchArray.getJSONObject(j);
                    int branchId = branchObject.getInt(KEY_ID);
                    String branch = branchObject.getString(KEY_NAME);
                    mEducationList.add(new Education(branchId, branch));
                }
            }
            mEducationArrayAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = response.getJSONArray(KEY_OCCUPATION_SECTOR);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String occupationSector = jsonObject.getString(KEY_NAME);
                JSONArray occupationArray = jsonObject.getJSONArray(KEY_OCCUPATION);
                for (int j = 0; j < occupationArray.length(); j++) {
                    JSONObject occupationObject = occupationArray.getJSONObject(j);
                    int occupationId = occupationObject.getInt(KEY_ID);
                    String occupation = occupationObject.getString(KEY_NAME);
                    mOccupationList.add(new Occupation(occupationId, occupation));
                }
            }
            mOccupationArrayAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processProfessionalDetails() {
        mCallback.launchNextScreen();
    }

    private void displayProfessionalDetails() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutFooter.setVisibility(View.VISIBLE);
        mLayoutProfessionalDetails.setVisibility(View.VISIBLE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
        mLayoutFooter.setVisibility(View.GONE);
        mLayoutProfessionalDetails.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayOops() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutFooter.setVisibility(View.GONE);
        mLayoutProfessionalDetails.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.VISIBLE);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
