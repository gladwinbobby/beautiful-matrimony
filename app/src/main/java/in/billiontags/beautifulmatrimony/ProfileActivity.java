package in.billiontags.beautifulmatrimony;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import in.billiontags.beautifulmatrimony.adapter.ThumbnailAdapter;
import in.billiontags.beautifulmatrimony.model.ThumbnailItem;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileActivity extends AppCompatActivity
        implements ThumbnailAdapter.ThumbnailClickListener {

    private Context mContext;
    private Toolbar mToolbar;
    private CircleImageView mImageViewProfile;
    private RecyclerView mRecyclerView;
    private List<ThumbnailItem> mThumbnailItemList;
    private ThumbnailAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initObjects();
        initToolbar();
        initRecyclerView();
        populateData();
        setSelection(0);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onThumbnailClick(int position) {
        setSelection(position);
    }

    private void initObjects() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mImageViewProfile = (CircleImageView) findViewById(R.id.img_profile);
        mRecyclerView = (RecyclerView) findViewById(R.id.thumbnail_list);

        mContext = this;
        mThumbnailItemList = new ArrayList<>();
        mAdapter = new ThumbnailAdapter(mContext, mThumbnailItemList, this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
        }
    }

    private void initRecyclerView() {
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(
                new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
    }

    private void populateData() {
        mThumbnailItemList.add(new ThumbnailItem(1, "http://i.imgur.com/CEGlxcp.jpg", false));
        mThumbnailItemList.add(new ThumbnailItem(2, "http://i.imgur.com/7DtMVjU.jpg", false));
        mThumbnailItemList.add(new ThumbnailItem(3, "http://i.imgur.com/xCM0CKx.jpg", false));
        mThumbnailItemList.add(new ThumbnailItem(4, "http://i.imgur.com/s5fGQkY.jpg", false));
        mThumbnailItemList.add(new ThumbnailItem(5, "http://i.imgur.com/TBhAZ3U.jpg", false));
        mAdapter.notifyDataSetChanged();
    }

    private void setSelection(int position) {
        for (int i = 0; i < mThumbnailItemList.size(); i++) {
            if (i == position) {
                mThumbnailItemList.get(i).setSelected(true);
            } else {
                mThumbnailItemList.get(i).setSelected(false);
            }
        }
        mAdapter.notifyDataSetChanged();
        Glide.with(mContext).load(mThumbnailItemList.get(position).getThumbnail()).into(
                mImageViewProfile);
    }
}
