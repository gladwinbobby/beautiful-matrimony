package in.billiontags.beautifulmatrimony;

import static in.billiontags.beautifulmatrimony.app.Activity.launch;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.widget.TextView;

import in.billiontags.beautifulmatrimony.app.MyPreference;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity {

    private Context mContext;
    private TextView mTextViewFooter;
    private MyPreference mPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initObjects();
        setFooterTextSpan();
        checkUserSession();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {
        mTextViewFooter = (TextView) findViewById(R.id.txt_footer);

        mContext = this;
        mPreference = new MyPreference(mContext);
    }

    private void setFooterTextSpan() {
        ImageSpan imageSpan = new ImageSpan(mContext, R.drawable.ic_heart,
                DynamicDrawableSpan.ALIGN_BASELINE);
        SpannableString string = new SpannableString("Made with    by Billiontags");
        string.setSpan(imageSpan, 10, 12, 0);
        string.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.primary)),
                16, 27, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTextViewFooter.setText(string);
    }

    private void checkUserSession() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mPreference.getToken() == null) {
                    launch(mContext, WelcomeActivity.class);
                } else {
                    launch(mContext, MainActivity.class);
                }
            }
        }, 5000);
    }
}
