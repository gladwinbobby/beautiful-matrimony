package in.billiontags.beautifulmatrimony;


import static in.billiontags.beautifulmatrimony.helper.Utils.getFamilyStatusList;
import static in.billiontags.beautifulmatrimony.helper.Utils.getFamilyValueList;
import static in.billiontags.beautifulmatrimony.helper.Utils.getHeightCmList;
import static in.billiontags.beautifulmatrimony.helper.Utils.getHeightFtList;
import static in.billiontags.beautifulmatrimony.helper.Utils.getMaritalStatusList;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;
import in.billiontags.beautifulmatrimony.model.FamilyStatus;
import in.billiontags.beautifulmatrimony.model.FamilyValue;
import in.billiontags.beautifulmatrimony.model.HeightCm;
import in.billiontags.beautifulmatrimony.model.HeightFt;
import in.billiontags.beautifulmatrimony.model.MaritalStatus;


/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalDetailsFragment extends Fragment implements AdapterView.OnItemSelectedListener,
        View.OnClickListener {

    private View mRootView;
    private Context mContext;
    private ProgressBar mProgressBar;
    private SearchableSpinner mSpinnerMaritalStatus, mSpinnerHeightFt, mSpinnerHeightCm,
            mSpinnerFamilyStatus, mSpinnerFamilyValue;
    private RadioGroup mGroupFamilyType, mGroupDisability;
    private Button mButtonSkip, mButtonContinue, mButtonRetry;
    private LinearLayout mLayoutPersonalDetails, mLayoutFooter, mLayoutOops;
    private List<MaritalStatus> mMaritalStatusList;
    private List<HeightFt> mHeightFtList;
    private List<HeightCm> mHeightCmList;
    private List<FamilyStatus> mFamilyStatusList;
    private List<FamilyValue> mFamilyValueList;
    private ArrayAdapter<MaritalStatus> mMaritalStatusArrayAdapter;
    private ArrayAdapter<HeightFt> mHeightFtArrayAdapter;
    private ArrayAdapter<HeightCm> mHeightCmArrayAdapter;
    private ArrayAdapter<FamilyStatus> mFamilyStatusArrayAdapter;
    private ArrayAdapter<FamilyValue> mFamilyValueArrayAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;

    public PersonalDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_personal_details, container, false);
        initObjects();
        initCallbacks();
        initSpinners();
        displayPersonalDetails();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == mSpinnerHeightFt) {
            float heightFt = mHeightFtList.get(position).getHeight();
            for (int i = 0; i < mHeightCmList.size(); i++) {
                float heightCm = mHeightCmList.get(i).getHeight();
                if (heightFt == heightCm) {
                    mSpinnerHeightCm.setSelection(i);
                    break;
                }
            }
        } else if (parent == mSpinnerHeightCm) {
            float heightCm = mHeightCmList.get(position).getHeight();
            for (int i = 0; i < mHeightFtList.size(); i++) {
                float heightFt = Float.parseFloat(
                        new DecimalFormat("#.0").format(mHeightFtList.get(i).getHeight()));
                if (heightFt == heightCm) {
                    mSpinnerHeightFt.setSelection(i);
                    break;
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSkip) {
            processPersonalDetails();
        } else if (v == mButtonContinue) {
            processPersonalDetails();
        }
    }

    private void initObjects() {
        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progress);
        mSpinnerMaritalStatus = (SearchableSpinner) mRootView.findViewById(
                R.id.spin_marital_status);
        mSpinnerHeightFt = (SearchableSpinner) mRootView.findViewById(R.id.spin_height_ft);
        mSpinnerHeightCm = (SearchableSpinner) mRootView.findViewById(R.id.spin_height_cm);
        mSpinnerFamilyStatus = (SearchableSpinner) mRootView.findViewById(R.id.spin_family_status);
        mSpinnerFamilyValue = (SearchableSpinner) mRootView.findViewById(R.id.spin_family_value);
        mGroupFamilyType = (RadioGroup) mRootView.findViewById(R.id.grp_family_type);
        mGroupDisability = (RadioGroup) mRootView.findViewById(R.id.grp_disability);
        mButtonSkip = (Button) mRootView.findViewById(R.id.btn_skip);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);
        mButtonRetry = (Button) mRootView.findViewById(R.id.btn_retry);
        mLayoutFooter = (LinearLayout) mRootView.findViewById(R.id.footer);
        mLayoutPersonalDetails = (LinearLayout) mRootView.findViewById(R.id.personal_details);
        mLayoutOops = (LinearLayout) mRootView.findViewById(R.id.oops);

        mContext = getActivity();
        mMaritalStatusList = new ArrayList<>();
        mHeightFtList = new ArrayList<>();
        mHeightCmList = new ArrayList<>();
        mFamilyStatusList = new ArrayList<>();
        mFamilyValueList = new ArrayList<>();
        mMaritalStatusArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mMaritalStatusList);
        mHeightFtArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mHeightFtList);
        mHeightCmArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mHeightCmList);
        mFamilyStatusArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mFamilyStatusList);
        mFamilyValueArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mFamilyValueList);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mSpinnerMaritalStatus.setOnItemSelectedListener(this);
        mSpinnerHeightFt.setOnItemSelectedListener(this);
        mSpinnerHeightCm.setOnItemSelectedListener(this);
        mButtonSkip.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
        mButtonRetry.setOnClickListener(this);
    }

    private void initSpinners() {
        mSpinnerMaritalStatus.setTitle("Select Marital Status");
        mSpinnerHeightFt.setTitle("Select Height");
        mSpinnerHeightCm.setTitle("Select Height");
        mSpinnerFamilyStatus.setTitle("Select Family Status");
        mSpinnerFamilyValue.setTitle("Select Family Value");

        mMaritalStatusList.addAll(getMaritalStatusList());
        mHeightFtList.addAll(getHeightFtList());
        mHeightCmList.addAll(getHeightCmList());
        mFamilyStatusList.addAll(getFamilyStatusList());
        mFamilyValueList.addAll(getFamilyValueList());

        mMaritalStatusArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerMaritalStatus.setAdapter(mMaritalStatusArrayAdapter);
        mHeightFtArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerHeightFt.setAdapter(mHeightFtArrayAdapter);
        mMaritalStatusArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerHeightCm.setAdapter(mHeightCmArrayAdapter);
        mFamilyStatusArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerFamilyStatus.setAdapter(mFamilyStatusArrayAdapter);
        mFamilyValueArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerFamilyValue.setAdapter(mFamilyValueArrayAdapter);
    }

    private void processPersonalDetails() {
        mCallback.launchNextScreen();
    }

    private void displayPersonalDetails() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutFooter.setVisibility(View.VISIBLE);
        mLayoutPersonalDetails.setVisibility(View.VISIBLE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
        mLayoutFooter.setVisibility(View.GONE);
        mLayoutPersonalDetails.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayOops() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutFooter.setVisibility(View.GONE);
        mLayoutPersonalDetails.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.VISIBLE);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
