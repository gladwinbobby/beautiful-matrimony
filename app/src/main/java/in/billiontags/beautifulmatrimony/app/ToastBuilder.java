package in.billiontags.beautifulmatrimony.app;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Bobby on 03/07/17
 */

public class ToastBuilder {
    public static void build(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
