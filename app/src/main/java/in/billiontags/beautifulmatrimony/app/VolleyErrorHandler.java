package in.billiontags.beautifulmatrimony.app;

import static in.billiontags.beautifulmatrimony.app.Activity.launchClearStack;

import android.content.Context;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import in.billiontags.beautifulmatrimony.R;
import in.billiontags.beautifulmatrimony.SplashActivity;

/**
 * Created by Bobby on 03/07/17
 */

public class VolleyErrorHandler {
    public static void handle(Context context, VolleyError volleyError) {
        if (volleyError instanceof NoConnectionError) {
            ToastBuilder.build(context, context.getString(R.string.error_no_internet));
        }

        NetworkResponse networkResponse = volleyError.networkResponse;
        if (networkResponse != null) {
            int statusCode = networkResponse.statusCode;
            String error = new String(networkResponse.data);
            processError(context, statusCode, error);
        }
    }

    public static void processError(Context context, int statusCode, String error) {
        MyPreference preference = new MyPreference(context);
        String message = null;
        try {
            JSONObject jsonObject = new JSONObject(error);
            if (jsonObject.has(Api.KEY_NON_FIELD_ERRORS)) {
                JSONArray jsonArray = jsonObject.getJSONArray(Api.KEY_NON_FIELD_ERRORS);
                message = jsonArray.getString(0);
            } else if (jsonObject.has(Api.KEY_DETAIL)) {
                message = jsonObject.getString(Api.KEY_DETAIL);
            } else {
                for (int i = 0; i < jsonObject.length(); i++) {
                    JSONArray keyArray = new JSONArray(
                            jsonObject.getString(jsonObject.names().getString(i)));
                    message = keyArray.getString(0);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        switch (statusCode) {
            case 400:
                if (message != null) {
                    ToastBuilder.build(context, message);
                }
                break;
            case 401:
                if (message != null) {
                    ToastBuilder.build(context, message);
                }
                preference.clearUser();
                launchClearStack(context, SplashActivity.class);
                break;
            case 403:
                if (message != null) {
                    ToastBuilder.build(context, message);
                }
                preference.clearUser();
                launchClearStack(context, SplashActivity.class);
                break;
            case 404:
                if (message != null) {
                    ToastBuilder.build(context, message);
                }
                break;
            default:
                ToastBuilder.build(context, context.getString(R.string.error_unexpected));
                break;
        }
    }
}
