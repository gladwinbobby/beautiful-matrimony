package in.billiontags.beautifulmatrimony.app;

/**
 * Created by Bobby on 03/07/17
 */

public class Api {
    /**
     * Keys
     */
    public static final String KEY_RELIGION = "religion";
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_RELATIONSHIP = "relationship";
    public static final String KEY_MOTHER_TONGUE = "mother_tongue";
    public static final String KEY_COUNTRY_CODE = "country_code";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_EDUCATION = "education";
    public static final String KEY_BRANCH = "branch";
    public static final String KEY_DOSHAM = "dosham";
    public static final String KEY_CURRENCY = "currency";
    public static final String KEY_OCCUPATION_SECTOR = "occupation_sector";
    public static final String KEY_OCCUPATION = "occupation";
    public static final String KEY_CASTE = "caste";
    public static final String KEY_COUNTRY = "country";
    public static final String KEY_HOBBIES = "hobbies";
    public static final String KEY_SPORTS = "sports";
    public static final String KEY_MUSIC = "music";
    public static final String KEY_LANGUAGES = "languages";
    public static final String KEY_STAR = "star";
    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_LAST_NAME = "last_name";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_USERDETAILS = "userdetails";
    public static final String KEY_DOB = "dob";
    public static final String KEY_USERPROFILE = "userprofile";
    public static final String KEY_GOOGLE_PROFILE_PIC = "google_profile_pic";
    public static final String KEY_FACEBOOK_PROFILE_PIC = "facebook_profile_pic";
    public static final String KEY_PROFILE_PIC = "profile_pic";
    public static final String KEY_LAST_UPDATED = "updated";
    public static final String KEY_UID = "uid";
    public static final String KEY_USER_ADDRESS = "user_address";
    public static final String KEY_IS_SHORTLISTED = "is_shortlisted";
    public static final String KEY_IS_IGNORED = "is_ignored";
    public static final String KEY_NEXT_URL = "next_url";
    public static final String KEY_COUNT = "count";
    public static final String KEY_PREV_URL = "prev_url";
    public static final String KEY_RESULTS = "results";
    public static final String KEY_STREET = "street";
    public static final String KEY_AREA = "area";
    public static final String KEY_CITY = "city";
    public static final String KEY_STATE = "state";
    public static final String KEY_ZIPCODE = "zipcode";
    /**
     * URLs
     */
    public static final String BASE_URL = "http://matrimony.billioncart.in/";
    public static final String AUTH = BASE_URL + "auth/";
    public static final String REGISTER = AUTH + "registration/";
    public static final String USER = BASE_URL + "user/";
    public static final String USER_LIST = USER + "list/";
    public static final String SUB_CASTE = USER_LIST + "sub-caste/";
    public static final String RAASI = USER_LIST + "raasi/";
    public static final String DATA_SIGN_UP = USER_LIST + "drop-down/sign-up/";
    public static final String DATA_PHASE_1 = USER_LIST + "drop-down/register-phase-1/";
    public static final String DATA_PHASE_4 = USER_LIST + "drop-down/register-phase-4/";
    public static final String DATA_PHASE_5 = USER_LIST + "drop-down/register-phase-5/";
    public static final String DATA_PHASE_6 = USER_LIST + "drop-down/register-phase-6/";
    public static final String GENDER_API_URL = "https://gender-api.com/get?name=";
    public static final String STATE = USER_LIST + "states/";
    public static final String CITY = USER_LIST + "cities/";
    public static final String MATRIMONY = USER_LIST + "matrimony/";
    static final String KEY_DETAIL = "detail";
    static final String KEY_NON_FIELD_ERRORS = "non_field_errors";
}
