package in.billiontags.beautifulmatrimony.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

/**
 * Created by Bobby on 02-05-2017
 */

public class MyPreference {

    private static final String PREF_USER = "user";
    private static final String NAME = "name";
    private static final String PHONE = "phone";
    private static final String EMAIL = "email";
    private static final String TOKEN = "token";
    private SharedPreferences mPreferencesUser;

    public MyPreference(Context context) {
        mPreferencesUser = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
    }

    public String getName() {
        return mPreferencesUser.getString(encode(NAME), null);
    }

    public void setName(String name) {
        mPreferencesUser.edit().putString(encode(NAME), name).apply();
    }

    public String getPhone() {
        return mPreferencesUser.getString(encode(PHONE), null);
    }

    public void setPhone(String phone) {
        mPreferencesUser.edit().putString(encode(PHONE), phone).apply();
    }

    public String getEmail() {
        return mPreferencesUser.getString(encode(EMAIL), null);
    }

    public void setEmail(String email) {
        mPreferencesUser.edit().putString(encode(EMAIL), email).apply();
    }

    public String getToken() {
        return mPreferencesUser.getString(encode(TOKEN), null);
    }

    public void setToken(String token) {
        mPreferencesUser.edit().putString(encode(TOKEN), token).apply();
    }

    public void clearUser() {
        mPreferencesUser.edit().clear().apply();
    }

    private String encode(String data) {
        return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);
    }
}
