package in.billiontags.beautifulmatrimony;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.adapter.SuggestionAdapter;
import in.billiontags.beautifulmatrimony.model.SuggestionItem;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment
        implements TabLayout.OnTabSelectedListener, SuggestionAdapter.SuggestionClickListener {

    private Context mContext;
    private View mRootView;
    private TabLayout mTabLayout;
    private RecyclerView mRecyclerView;
    private List<Object> mObjectList;
    private LinearLayoutManager mLayoutManager;
    private SuggestionAdapter mAdapter;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_search, container, false);
        initObjects();
        initCallbacks();
        initTabs();
        setTabsFont();
        initRecyclerView();
        populateData();
        return mRootView;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onSuggestionClick(int id) {

    }

    private void initObjects() {
        mTabLayout = (TabLayout) mRootView.findViewById(R.id.tab);
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.suggestion_list);

        mContext = getActivity();
        mObjectList = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mAdapter = new SuggestionAdapter(mContext, mObjectList, this);
    }

    private void initCallbacks() {
        mTabLayout.addOnTabSelectedListener(this);
    }

    private void initTabs() {
        mTabLayout.addTab(mTabLayout.newTab().setText("Regular Search"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Advanced Search"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Search by Date"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Soulmate Search"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Keyword Search"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Recently Viewed Profiles"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Search by ID"));
    }

    private void setTabsFont() {
        ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(
                            Typeface.createFromAsset(mContext.getAssets(), "fonts/coves_light.otf"),
                            Typeface.NORMAL);
                    ((TextView) tabViewChild).setTextSize(
                            getResources().getDimension(R.dimen.small_txt));
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void populateData() {
        mObjectList.add(new SuggestionItem(1, "F2748172", "http://i.imgur.com/7DtMVjU.jpg"));
        mObjectList.add(new SuggestionItem(2, "F1872472", "http://i.imgur.com/CEGlxcp.jpg"));
        mObjectList.add(new SuggestionItem(3, "F87214581", "http://i.imgur.com/xCM0CKx.jpg"));
        mObjectList.add(new SuggestionItem(4, "F87215481", "http://i.imgur.com/s5fGQkY.jpg"));
        mObjectList.add(new SuggestionItem(5, "F872487234", "http://i.imgur.com/TBhAZ3U.jpg"));
        mAdapter.notifyDataSetChanged();
    }
}
