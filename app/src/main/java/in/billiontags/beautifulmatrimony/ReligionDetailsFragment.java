package in.billiontags.beautifulmatrimony;


import static in.billiontags.beautifulmatrimony.app.Api.DATA_PHASE_1;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_CASTE;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_DOSHAM;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_ID;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_NAME;
import static in.billiontags.beautifulmatrimony.app.Api.SUB_CASTE;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.billiontags.beautifulmatrimony.adapter.DoshamAdapter;
import in.billiontags.beautifulmatrimony.app.AppController;
import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.app.VolleyErrorHandler;
import in.billiontags.beautifulmatrimony.callback.DoshamCallback;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;
import in.billiontags.beautifulmatrimony.model.Caste;
import in.billiontags.beautifulmatrimony.model.Dosham;
import in.billiontags.beautifulmatrimony.model.SubCaste;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReligionDetailsFragment extends Fragment implements View.OnClickListener,
        AdapterView.OnItemSelectedListener, DoshamCallback, RadioGroup.OnCheckedChangeListener {

    private View mRootView;
    private Context mContext;
    private ProgressBar mProgressBar;
    private SearchableSpinner mSpinnerCaste, mSpinnerSubCaste;
    private EditText mEditTextGothra;
    private RadioGroup mGroupDosham;
    private RecyclerView mRecyclerView;
    private Button mButtonSkip, mButtonContinue, mButtonRetry;
    private LinearLayout mLayoutReligionDetails, mLayoutFooter, mLayoutOops;
    private List<Caste> mCasteList;
    private List<SubCaste> mSubCasteList;
    private List<Dosham> mDoshamList;
    private ArrayAdapter<Caste> mCasteArrayAdapter;
    private ArrayAdapter<SubCaste> mSubCasteArrayAdapter;
    private DoshamAdapter mDoshamAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;

    public ReligionDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_religion_details, container, false);
        initObjects();
        initCallbacks();
        initSpinners();
        initRecyclerView();
        displayLoading();
        getData();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSkip) {
            processReligionDetails();
        } else if (v == mButtonContinue) {
            processReligionDetails();
        } else if (v == mButtonRetry) {
            displayLoading();
            getData();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == mSpinnerCaste) {
            getSubCaste(mCasteList.get(position).getId());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onDoshamClick(int position) {
        Dosham dosham = mDoshamList.get(position);
        dosham.setSelected(!dosham.isSelected());
        mDoshamAdapter.notifyItemChanged(position);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        if (checkedId == R.id.radio_yes) {
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    private void initObjects() {
        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progress);
        mSpinnerCaste = (SearchableSpinner) mRootView.findViewById(R.id.spin_caste);
        mSpinnerSubCaste = (SearchableSpinner) mRootView.findViewById(R.id.spin_sub_caste);
        mEditTextGothra = (EditText) mRootView.findViewById(R.id.input_gothra);
        mGroupDosham = (RadioGroup) mRootView.findViewById(R.id.grp_dosham);
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.dosham);
        mButtonSkip = (Button) mRootView.findViewById(R.id.btn_skip);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);
        mButtonRetry = (Button) mRootView.findViewById(R.id.btn_retry);
        mLayoutFooter = (LinearLayout) mRootView.findViewById(R.id.footer);
        mLayoutReligionDetails = (LinearLayout) mRootView.findViewById(R.id.religion_details);
        mLayoutOops = (LinearLayout) mRootView.findViewById(R.id.oops);

        mContext = getActivity();
        mCasteList = new ArrayList<>();
        mSubCasteList = new ArrayList<>();
        mDoshamList = new ArrayList<>();
        mCasteArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text, mCasteList);
        mSubCasteArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mSubCasteList);
        mDoshamAdapter = new DoshamAdapter(mDoshamList, this);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mSpinnerCaste.setOnItemSelectedListener(this);
        mGroupDosham.setOnCheckedChangeListener(this);
        mButtonSkip.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
        mButtonRetry.setOnClickListener(this);
    }

    private void initSpinners() {
        mSpinnerCaste.setTitle("Select Caste");
        mSpinnerSubCaste.setTitle("Select Sub-Caste");
        mCasteArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerCaste.setAdapter(mCasteArrayAdapter);
        mSubCasteArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerSubCaste.setAdapter(mSubCasteArrayAdapter);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        mRecyclerView.setAdapter(mDoshamAdapter);
    }

    private void clearData() {
        mCasteList.clear();
        mSubCasteList.clear();
    }

    private void getData() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(DATA_PHASE_1, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayReligionDetails();
                        clearData();
                        handleDataResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                displayOops();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "data_phase1");
    }

    private void handleDataResponse(JSONObject response) {
        try {
            JSONArray jsonArray = response.getJSONArray(KEY_CASTE);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String caste = jsonObject.getString(KEY_NAME);
                mCasteList.add(new Caste(id, caste));
            }
            mCasteArrayAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = response.getJSONArray(KEY_DOSHAM);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String caste = jsonObject.getString(KEY_NAME);
                mDoshamList.add(new Dosham(id, caste, false));
            }
            mDoshamAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getSubCaste(int id) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(SUB_CASTE + id + "/",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        mSubCasteList.clear();
                        handleSubCasteResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonArrayRequest, "sub_caste");
    }

    private void handleSubCasteResponse(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jsonObject = response.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String subCaste = jsonObject.getString(KEY_NAME);
                mSubCasteList.add(new SubCaste(id, subCaste));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mSubCasteArrayAdapter.notifyDataSetChanged();
    }

    private void processReligionDetails() {
        String gothra = mEditTextGothra.getText().toString().trim();
        if (validateInput(gothra)) {
            mCallback.launchNextScreen();
        }
    }

    private boolean validateInput(String gothra) {
        if (gothra.isEmpty()) {
            mEditTextGothra.requestFocus();
            mEditTextGothra.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Gothra(m)"));
            return false;
        }
        return true;
    }

    private void displayReligionDetails() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutFooter.setVisibility(View.VISIBLE);
        mLayoutReligionDetails.setVisibility(View.VISIBLE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
        mLayoutFooter.setVisibility(View.GONE);
        mLayoutReligionDetails.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayOops() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutFooter.setVisibility(View.GONE);
        mLayoutReligionDetails.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.VISIBLE);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
