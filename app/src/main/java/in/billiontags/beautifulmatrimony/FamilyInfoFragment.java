package in.billiontags.beautifulmatrimony;


import static in.billiontags.beautifulmatrimony.helper.Utils.getFatherStatusList;
import static in.billiontags.beautifulmatrimony.helper.Utils.getMotherStatusList;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;
import in.billiontags.beautifulmatrimony.model.FatherStatus;
import in.billiontags.beautifulmatrimony.model.MotherStatus;


/**
 * A simple {@link Fragment} subclass.
 */
public class FamilyInfoFragment extends Fragment implements View.OnClickListener {

    private View mRootView;
    private Context mContext;
    private SearchableSpinner mSpinnerFatherStatus, mSpinnerMotherStatus;
    private EditText mEditTextParentContact;
    private Button mButtonSkip, mButtonContinue;
    private List<FatherStatus> mFatherStatusList;
    private List<MotherStatus> mMotherStatusList;
    private ArrayAdapter<FatherStatus> mFatherStatusArrayAdapter;
    private ArrayAdapter<MotherStatus> mMotherStatusArrayAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;

    public FamilyInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_family_info, container, false);
        initObjects();
        initCallbacks();
        initSpinners();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSkip) {
            mCallback.launchNextScreen();
        } else if (v == mButtonContinue) {
            mCallback.launchNextScreen();
        }
    }

    private void initObjects() {
        mSpinnerFatherStatus = (SearchableSpinner) mRootView.findViewById(R.id.spin_father_status);
        mSpinnerMotherStatus = (SearchableSpinner) mRootView.findViewById(R.id.spin_mother_status);
        mEditTextParentContact = (EditText) mRootView.findViewById(R.id.input_parent_contact);
        mButtonSkip = (Button) mRootView.findViewById(R.id.btn_skip);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);

        mContext = getActivity();
        mFatherStatusList = new ArrayList<>();
        mMotherStatusList = new ArrayList<>();
        mFatherStatusArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mFatherStatusList);
        mMotherStatusArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mMotherStatusList);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonSkip.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
    }

    private void initSpinners() {
        mSpinnerFatherStatus.setTitle("Select Father Status");
        mSpinnerMotherStatus.setTitle("Select Mother Status");

        mFatherStatusList.addAll(getFatherStatusList());
        mMotherStatusList.addAll(getMotherStatusList());

        mFatherStatusArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerFatherStatus.setAdapter(mFatherStatusArrayAdapter);
        mMotherStatusArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerMotherStatus.setAdapter(mMotherStatusArrayAdapter);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
