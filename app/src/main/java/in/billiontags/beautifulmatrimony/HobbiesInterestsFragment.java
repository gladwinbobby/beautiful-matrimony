package in.billiontags.beautifulmatrimony;


import static in.billiontags.beautifulmatrimony.app.Api.DATA_PHASE_4;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_HOBBIES;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_ID;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_LANGUAGES;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_MUSIC;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_NAME;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_SPORTS;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.adapter.FavoriteMusicAdapter;
import in.billiontags.beautifulmatrimony.adapter.HobbiesInterestsAdapter;
import in.billiontags.beautifulmatrimony.adapter.SpokenLanguageAdapter;
import in.billiontags.beautifulmatrimony.adapter.SportsFitnessAdapter;
import in.billiontags.beautifulmatrimony.app.AppController;
import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.app.VolleyErrorHandler;
import in.billiontags.beautifulmatrimony.callback.FavoriteMusicCallback;
import in.billiontags.beautifulmatrimony.callback.HobbiesInterestsCallback;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;
import in.billiontags.beautifulmatrimony.callback.SpokenLanguageCallback;
import in.billiontags.beautifulmatrimony.callback.SportsFitnessCallback;
import in.billiontags.beautifulmatrimony.model.Hobby;
import in.billiontags.beautifulmatrimony.model.Language;
import in.billiontags.beautifulmatrimony.model.Music;
import in.billiontags.beautifulmatrimony.model.Sport;


/**
 * A simple {@link Fragment} subclass.
 */
public class HobbiesInterestsFragment extends Fragment implements View.OnClickListener,
        HobbiesInterestsCallback, FavoriteMusicCallback, SportsFitnessCallback,
        SpokenLanguageCallback {

    private View mRootView;
    private Context mContext;
    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerViewHobby, mRecyclerViewMusic, mRecyclerViewSport,
            mRecyclerViewLanguage;
    private Button mButtonSkip, mButtonContinue, mButtonRetry;
    private LinearLayout mLayoutHobbies, mLayoutFooter, mLayoutOops;
    private List<Hobby> mHobbyList;
    private List<Music> mMusicList;
    private List<Sport> mSportList;
    private List<Language> mLanguageList;
    private HobbiesInterestsAdapter mInterestsAdapter;
    private FavoriteMusicAdapter mMusicAdapter;
    private SportsFitnessAdapter mFitnessAdapter;
    private SpokenLanguageAdapter mLanguageAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;

    public HobbiesInterestsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_hobbies_interests, container, false);
        initObjects();
        initCallbacks();
        initRecyclerView();
        showProgressDialog("Loading..");
        getData();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSkip) {
            mCallback.launchNextScreen();
        } else if (v == mButtonContinue) {
            mCallback.launchNextScreen();
        }
    }

    @Override
    public void onFavoriteMusicClick(int position) {
        Music music = mMusicList.get(position);
        music.setSelected(!music.isSelected());
        mMusicAdapter.notifyItemChanged(position);
    }

    @Override
    public void onSportsFitnessClick(int position) {
        Sport sport = mSportList.get(position);
        sport.setSelected(!sport.isSelected());
        mFitnessAdapter.notifyItemChanged(position);
    }

    @Override
    public void onSpokenLanguageClick(int position) {
        Language language = mLanguageList.get(position);
        language.setSelected(!language.isSelected());
        mLanguageAdapter.notifyItemChanged(position);
    }

    @Override
    public void onHobbiesInterestsClick(int position) {
        Hobby hobby = mHobbyList.get(position);
        hobby.setSelected(!hobby.isSelected());
        mInterestsAdapter.notifyItemChanged(position);
    }

    private void initObjects() {
        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progress);
        mRecyclerViewHobby = (RecyclerView) mRootView.findViewById(R.id.hobbies_interests);
        mRecyclerViewMusic = (RecyclerView) mRootView.findViewById(R.id.favorite_music);
        mRecyclerViewSport = (RecyclerView) mRootView.findViewById(R.id.sports_fitness);
        mRecyclerViewLanguage = (RecyclerView) mRootView.findViewById(R.id.spoken_languages);
        mButtonSkip = (Button) mRootView.findViewById(R.id.btn_skip);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);
        mButtonRetry = (Button) mRootView.findViewById(R.id.btn_retry);
        mLayoutFooter = (LinearLayout) mRootView.findViewById(R.id.footer);
        mLayoutHobbies = (LinearLayout) mRootView.findViewById(R.id.hobbies);
        mLayoutOops = (LinearLayout) mRootView.findViewById(R.id.oops);

        mContext = getActivity();
        mHobbyList = new ArrayList<>();
        mMusicList = new ArrayList<>();
        mSportList = new ArrayList<>();
        mLanguageList = new ArrayList<>();
        mInterestsAdapter = new HobbiesInterestsAdapter(mHobbyList, this);
        mMusicAdapter = new FavoriteMusicAdapter(mMusicList, this);
        mFitnessAdapter = new SportsFitnessAdapter(mSportList, this);
        mLanguageAdapter = new SpokenLanguageAdapter(mLanguageList, this);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonSkip.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
        mButtonRetry.setOnClickListener(this);
    }

    private void initRecyclerView() {
        mRecyclerViewHobby.setLayoutManager(new GridLayoutManager(mContext, 2));
        mRecyclerViewHobby.setAdapter(mInterestsAdapter);
        mRecyclerViewMusic.setLayoutManager(new GridLayoutManager(mContext, 2));
        mRecyclerViewMusic.setAdapter(mMusicAdapter);
        mRecyclerViewSport.setLayoutManager(new GridLayoutManager(mContext, 2));
        mRecyclerViewSport.setAdapter(mFitnessAdapter);
        mRecyclerViewLanguage.setLayoutManager(new GridLayoutManager(mContext, 2));
        mRecyclerViewLanguage.setAdapter(mLanguageAdapter);
    }

    private void clearData() {
        mHobbyList.clear();
        mMusicList.clear();
        mSportList.clear();
        mLanguageList.clear();
    }

    private void getData() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(DATA_PHASE_4, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgressDialog();
                        clearData();
                        handleDataResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "data_phase4");
    }

    private void handleDataResponse(JSONObject response) {
        try {
            JSONArray jsonArray = response.getJSONArray(KEY_HOBBIES);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String hobby = jsonObject.getString(KEY_NAME);
                mHobbyList.add(new Hobby(id, hobby, false));
            }
            mInterestsAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = response.getJSONArray(KEY_MUSIC);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String music = jsonObject.getString(KEY_NAME);
                mMusicList.add(new Music(id, music, false));
            }
            mMusicAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = response.getJSONArray(KEY_SPORTS);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String sport = jsonObject.getString(KEY_NAME);
                mSportList.add(new Sport(id, sport, false));
            }
            mFitnessAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = response.getJSONArray(KEY_LANGUAGES);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String lanugage = jsonObject.getString(KEY_NAME);
                mLanguageList.add(new Language(id, lanugage, false));
            }
            mLanguageAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void displayHobbies() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutFooter.setVisibility(View.VISIBLE);
        mLayoutHobbies.setVisibility(View.VISIBLE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
        mLayoutFooter.setVisibility(View.GONE);
        mLayoutHobbies.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayOops() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutFooter.setVisibility(View.GONE);
        mLayoutHobbies.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.VISIBLE);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
