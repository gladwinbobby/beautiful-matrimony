package in.billiontags.beautifulmatrimony;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.adapter.ConversationAdapter;
import in.billiontags.beautifulmatrimony.model.MessageItem;
import in.billiontags.beautifulmatrimony.model.ResponseItem;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ConversationActivity extends AppCompatActivity {

    private Context mContext;
    private Toolbar mToolbar;
    private TextView mTextViewIdentity;
    private RecyclerView mRecyclerView;
    private List<Object> mObjectList;
    private LinearLayoutManager mLayoutManager;
    private ConversationAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        initObjects();
        initToolbar();
        setIdentity();
        initRecyclerView();
        populateData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initObjects() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTextViewIdentity = (TextView) findViewById(R.id.txt_identity);
        mRecyclerView = (RecyclerView) findViewById(R.id.conversation_list);

        mContext = this;
        mObjectList = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(mContext);
        mAdapter = new ConversationAdapter(mContext, mObjectList);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
        }
    }

    private void setIdentity() {
        mTextViewIdentity.setText("Olivia Holt");
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void populateData() {
        mObjectList.add(new MessageItem(1, "Hi", "http://i.imgur.com/7DtMVjU.jpg", "Today", true));
        mObjectList.add(new ResponseItem(2, "Hello", "Today"));
        mObjectList.add(
                new MessageItem(3, "How are you?", "http://i.imgur.com/7DtMVjU.jpg", "Today",
                        true));
        mObjectList.add(new ResponseItem(4, "I\'m fine. How are you?", "Today"));
        mObjectList.add(
                new MessageItem(5, "I'm good", "http://i.imgur.com/7DtMVjU.jpg", "Today", true));
        mObjectList.add(new ResponseItem(6, "Wassup?", "Today"));
        mObjectList.add(new MessageItem(7, "Bye! Catch you later", "http://i.imgur.com/7DtMVjU.jpg",
                "Today", true));
        mObjectList.add(new ResponseItem(8, "Bye", "Today"));
        mAdapter.notifyDataSetChanged();
    }
}
