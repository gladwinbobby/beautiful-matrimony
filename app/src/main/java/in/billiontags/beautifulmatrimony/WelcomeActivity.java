package in.billiontags.beautifulmatrimony;

import static in.billiontags.beautifulmatrimony.app.Activity.launch;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WelcomeActivity extends AppCompatActivity
        implements View.OnClickListener {

    private Context mContext;
    private TextView mTextViewLogin, mTextViewRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        initObjects();
        initCallbacks();
        setLoginTextSpan();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        if (v == mTextViewLogin) {
            launch(mContext, MainActivity.class);
        } else if (v == mTextViewRegister) {
            launch(mContext, RegisterActivity.class);
        }
    }

    private void initObjects() {
        mTextViewLogin = (TextView) findViewById(R.id.txt_login);
        mTextViewRegister = (TextView) findViewById(R.id.txt_register);

        mContext = this;
    }

    private void initCallbacks() {
        mTextViewLogin.setOnClickListener(this);
        mTextViewRegister.setOnClickListener(this);
    }

    private void setLoginTextSpan() {
        SpannableString string = new SpannableString("Already have an account?    Login");
        string.setSpan(
                new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.primary)),
                28, 33, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTextViewLogin.setText(string);
    }
}
