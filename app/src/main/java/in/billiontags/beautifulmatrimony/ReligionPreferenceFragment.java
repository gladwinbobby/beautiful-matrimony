package in.billiontags.beautifulmatrimony;


import static in.billiontags.beautifulmatrimony.app.Api.DATA_PHASE_6;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_CASTE;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_ID;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_MOTHER_TONGUE;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_NAME;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_RELIGION;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_STAR;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.app.AppController;
import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.app.VolleyErrorHandler;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;
import in.billiontags.beautifulmatrimony.model.Caste;
import in.billiontags.beautifulmatrimony.model.MotherTongue;
import in.billiontags.beautifulmatrimony.model.Religion;
import in.billiontags.beautifulmatrimony.model.Star;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReligionPreferenceFragment extends Fragment implements View.OnClickListener {

    private View mRootView;
    private Context mContext;
    private SearchableSpinner mSpinnerReligion, mSpinnerMotherTongue, mSpinnerCaste, mSpinnerStar;
    private CheckBox mCheckBoxOtherCaste;
    private RadioGroup mGroupManglik;
    private Button mButtonSkip, mButtonContinue;
    private List<Religion> mReligionList;
    private List<MotherTongue> mMotherTongueList;
    private List<Caste> mCasteList;
    private List<Star> mStarList;
    private ArrayAdapter<Religion> mReligionArrayAdapter;
    private ArrayAdapter<MotherTongue> mMotherTongueArrayAdapter;
    private ArrayAdapter<Caste> mCasteArrayAdapter;
    private ArrayAdapter<Star> mStarArrayAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;

    public ReligionPreferenceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_religion_preference, container, false);
        initObjects();
        initCallbacks();
        initSpinners();
        showProgressDialog("Loading..");
        getData();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSkip) {
            mCallback.launchNextScreen();
        } else if (v == mButtonContinue) {
            mCallback.launchNextScreen();
        }
    }

    private void initObjects() {
        mSpinnerReligion = (SearchableSpinner) mRootView.findViewById(R.id.spin_religion);
        mSpinnerMotherTongue = (SearchableSpinner) mRootView.findViewById(R.id.spin_mother_tongue);
        mSpinnerCaste = (SearchableSpinner) mRootView.findViewById(R.id.spin_caste);
        mSpinnerStar = (SearchableSpinner) mRootView.findViewById(R.id.spin_star);
        mCheckBoxOtherCaste = (CheckBox) mRootView.findViewById(R.id.check_other_caste);
        mGroupManglik = (RadioGroup) mRootView.findViewById(R.id.grp_manglik);
        mButtonSkip = (Button) mRootView.findViewById(R.id.btn_skip);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);

        mContext = getActivity();
        mReligionList = new ArrayList<>();
        mMotherTongueList = new ArrayList<>();
        mCasteList = new ArrayList<>();
        mStarList = new ArrayList<>();
        mReligionArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mReligionList);
        mMotherTongueArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mMotherTongueList);
        mCasteArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text, mCasteList);
        mStarArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text, mStarList);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonSkip.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
    }

    private void initSpinners() {
        mSpinnerReligion.setTitle("Select Religion");
        mSpinnerMotherTongue.setTitle("Select Mother Tongue");
        mSpinnerCaste.setTitle("Select Caste");
        mSpinnerStar.setTitle("Select Star");
        mReligionArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerReligion.setAdapter(mReligionArrayAdapter);
        mMotherTongueArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerMotherTongue.setAdapter(mMotherTongueArrayAdapter);
        mCasteArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerCaste.setAdapter(mCasteArrayAdapter);
        mStarArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerStar.setAdapter(mStarArrayAdapter);
    }

    private void clearData() {
        mReligionList.clear();
        mMotherTongueList.clear();
        mCasteList.clear();
        mStarList.clear();
    }

    private void getData() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(DATA_PHASE_6, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgressDialog();
                        clearData();
                        handleDataResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "data_phase6");
    }

    private void handleDataResponse(JSONObject response) {
        try {
            JSONArray jsonArray = response.getJSONArray(KEY_RELIGION);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String religion = jsonObject.getString(KEY_NAME);
                mReligionList.add(new Religion(id, religion));
            }
            mReligionArrayAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = response.getJSONArray(KEY_MOTHER_TONGUE);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String motherTongue = jsonObject.getString(KEY_NAME);
                mMotherTongueList.add(new MotherTongue(id, motherTongue));
            }
            mMotherTongueArrayAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = response.getJSONArray(KEY_CASTE);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String caste = jsonObject.getString(KEY_NAME);
                mCasteList.add(new Caste(id, caste));
            }
            mCasteArrayAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = response.getJSONArray(KEY_STAR);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String star = jsonObject.getString(KEY_NAME);
                mStarList.add(new Star(id, star));
            }
            mStarArrayAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
