package in.billiontags.beautifulmatrimony;


import static in.billiontags.beautifulmatrimony.app.Api.CITY;
import static in.billiontags.beautifulmatrimony.app.Api.DATA_PHASE_1;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_COUNTRY;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_ID;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_NAME;
import static in.billiontags.beautifulmatrimony.app.Api.STATE;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.billiontags.beautifulmatrimony.app.AppController;
import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.app.VolleyErrorHandler;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;
import in.billiontags.beautifulmatrimony.model.City;
import in.billiontags.beautifulmatrimony.model.Country;
import in.billiontags.beautifulmatrimony.model.State;


/**
 * A simple {@link Fragment} subclass.
 */
public class CurrentLocationFragment extends Fragment implements View.OnClickListener,
        AdapterView.OnItemSelectedListener {

    private View mRootView;
    private Context mContext;
    private ProgressBar mProgressBar;
    private SearchableSpinner mSpinnerCountry, mSpinnerState, mSpinnerCity;
    private EditText mEditTextStreet, mEditTextArea, mEditTextZipcode;
    private Button mButtonSkip, mButtonContinue, mButtonRetry;
    private LinearLayout mLayoutFooter, mLayoutCurrentLocation, mLayoutOops;
    private List<Country> mCountryList;
    private List<State> mStateList;
    private List<City> mCityList;
    private ArrayAdapter<Country> mCountryArrayAdapter;
    private ArrayAdapter<State> mStateArrayAdapter;
    private ArrayAdapter<City> mCityArrayAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;

    public CurrentLocationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_current_location, container, false);
        initObjects();
        initCallbacks();
        initSpinners();
        displayLoading();
        getCountries();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSkip) {
            processCurrentLocation();
        } else if (v == mButtonContinue) {
            processCurrentLocation();
        } else if (v == mButtonRetry) {
            displayLoading();
            getCountries();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == mSpinnerCountry) {
            Country country = mCountryList.get(mSpinnerCountry.getSelectedItemPosition());
            getState(country.getId());
        } else if (parent == mSpinnerState) {
            State state = mStateList.get(mSpinnerState.getSelectedItemPosition());
            getCity(state.getId());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void initObjects() {
        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progress);
        mSpinnerCountry = (SearchableSpinner) mRootView.findViewById(R.id.spin_country);
        mSpinnerState = (SearchableSpinner) mRootView.findViewById(R.id.spin_state);
        mSpinnerCity = (SearchableSpinner) mRootView.findViewById(R.id.spin_city);
        mEditTextStreet = (EditText) mRootView.findViewById(R.id.input_street);
        mEditTextArea = (EditText) mRootView.findViewById(R.id.input_area);
        mEditTextZipcode = (EditText) mRootView.findViewById(R.id.input_zipcode);
        mButtonSkip = (Button) mRootView.findViewById(R.id.btn_skip);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);
        mButtonRetry = (Button) mRootView.findViewById(R.id.btn_retry);
        mLayoutFooter = (LinearLayout) mRootView.findViewById(R.id.footer);
        mLayoutCurrentLocation = (LinearLayout) mRootView.findViewById(R.id.current_location);
        mLayoutOops = (LinearLayout) mRootView.findViewById(R.id.oops);

        mContext = getActivity();
        mCountryList = new ArrayList<>();
        mStateList = new ArrayList<>();
        mCityList = new ArrayList<>();
        mCountryArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mCountryList);
        mStateArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text, mStateList);
        mCityArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text, mCityList);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mSpinnerCountry.setOnItemSelectedListener(this);
        mSpinnerState.setOnItemSelectedListener(this);
        mButtonSkip.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
        mButtonRetry.setOnClickListener(this);
    }

    private void initSpinners() {
        mSpinnerCountry.setTitle("Select Country");
        mSpinnerState.setTitle("Select State");
        mSpinnerCity.setTitle("Select City");
        mCountryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerCountry.setAdapter(mCountryArrayAdapter);
        mStateArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerState.setAdapter(mStateArrayAdapter);
        mCityArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerCity.setAdapter(mCityArrayAdapter);
    }

    private void getCountries() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(DATA_PHASE_1, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayCurrentLocation();
                        mCountryList.clear();
                        handleCountryResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                displayOops();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "country");
    }

    private void handleCountryResponse(JSONObject response) {
        try {
            JSONArray jsonArray = response.getJSONArray(KEY_COUNTRY);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String country = jsonObject.getString(KEY_NAME);
                mCountryList.add(new Country(id, country));
            }
            mCountryArrayAdapter.notifyDataSetChanged();
            mSpinnerCountry.setSelection(97);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getState(int id) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(STATE + id + "/",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        mStateList.clear();
                        mCityList.clear();
                        handleStateResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonArrayRequest, "state");
    }

    private void handleStateResponse(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jsonObject = response.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String state = jsonObject.getString(KEY_NAME);
                mStateList.add(new State(id, state));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mStateArrayAdapter.notifyDataSetChanged();
    }

    private void getCity(int id) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(CITY + id + "/",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        mCityList.clear();
                        handleCityResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonArrayRequest, "city");
    }

    private void handleCityResponse(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jsonObject = response.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String city = jsonObject.getString(KEY_NAME);
                mCityList.add(new City(id, city));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mCityArrayAdapter.notifyDataSetChanged();
    }

    private void processCurrentLocation() {
        String street = mEditTextStreet.getText().toString().trim();
        String area = mEditTextArea.getText().toString().trim();
        String zipCode = mEditTextZipcode.getText().toString().trim();
        if (validateInput(street, area, zipCode)) {
            mCallback.launchNextScreen();
        }
    }

    private boolean validateInput(String street, String area, String zipCode) {
        if (street.isEmpty()) {
            mEditTextStreet.requestFocus();
            mEditTextStreet.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty), "Street"));
            return false;
        } else if (area.isEmpty()) {
            mEditTextArea.requestFocus();
            mEditTextArea.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty), "Area"));
            return false;
        } else if (zipCode.isEmpty()) {
            mEditTextZipcode.requestFocus();
            mEditTextZipcode.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty), "Zipcode"));
            return false;
        }
        return true;
    }

    private void displayCurrentLocation() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutFooter.setVisibility(View.VISIBLE);
        mLayoutCurrentLocation.setVisibility(View.VISIBLE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
        mLayoutFooter.setVisibility(View.GONE);
        mLayoutCurrentLocation.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayOops() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutFooter.setVisibility(View.GONE);
        mLayoutCurrentLocation.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.VISIBLE);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
