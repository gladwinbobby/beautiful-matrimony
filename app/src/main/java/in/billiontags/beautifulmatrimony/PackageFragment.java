package in.billiontags.beautifulmatrimony;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rodolfonavalon.shaperipplelibrary.ShapeRipple;


/**
 * A simple {@link Fragment} subclass.
 */
public class PackageFragment extends Fragment {

    private Context mContext;
    private View mRootView;
    private ShapeRipple mShapeRipple;

    public PackageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_package, container, false);
        initObjects();
        return mRootView;
    }

    private void initObjects() {
        mShapeRipple = (ShapeRipple) mRootView.findViewById(R.id.ripple);

        mContext = getActivity();
    }
}
