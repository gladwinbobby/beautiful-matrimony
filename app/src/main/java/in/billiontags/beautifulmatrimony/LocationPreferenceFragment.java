package in.billiontags.beautifulmatrimony;


import static in.billiontags.beautifulmatrimony.app.Api.DATA_PHASE_6;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_COUNTRY;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_ID;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_NAME;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.app.AppController;
import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.app.VolleyErrorHandler;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;
import in.billiontags.beautifulmatrimony.model.Citizenship;
import in.billiontags.beautifulmatrimony.model.Country;


/**
 * A simple {@link Fragment} subclass.
 */
public class LocationPreferenceFragment extends Fragment implements View.OnClickListener {

    private View mRootView;
    private Context mContext;
    private SearchableSpinner mSpinnerCountry, mSpinnerCitizenship;
    private Button mButtonSkip, mButtonContinue;
    private List<Country> mCountryList;
    private List<Citizenship> mCitizenshipList;
    private ArrayAdapter<Country> mCountryArrayAdapter;
    private ArrayAdapter<Citizenship> mCitizenshipArrayAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;

    public LocationPreferenceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_location_preference, container, false);
        initObjects();
        initCallbacks();
        initSpinners();
        showProgressDialog("Loading..");
        getData();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSkip) {
            mCallback.launchNextScreen();
        } else if (v == mButtonContinue) {
            mCallback.launchNextScreen();
        }
    }

    private void initObjects() {
        mSpinnerCountry = (SearchableSpinner) mRootView.findViewById(R.id.spin_country);
        mSpinnerCitizenship = (SearchableSpinner) mRootView.findViewById(R.id.spin_citizenship);
        mButtonSkip = (Button) mRootView.findViewById(R.id.btn_skip);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);

        mContext = getActivity();
        mCountryList = new ArrayList<>();
        mCitizenshipList = new ArrayList<>();
        mCountryArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mCountryList);
        mCitizenshipArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mCitizenshipList);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonSkip.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
    }

    private void initSpinners() {
        mSpinnerCountry.setTitle("Select Country");
        mSpinnerCitizenship.setTitle("Select Citizenship");
        mCountryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerCountry.setAdapter(mCountryArrayAdapter);
        mCitizenshipArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerCitizenship.setAdapter(mCitizenshipArrayAdapter);
    }

    private void clearData() {
        mCountryList.clear();
        mCitizenshipList.clear();
    }

    private void getData() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(DATA_PHASE_6, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgressDialog();
                        clearData();
                        handleDataResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "data_phase6");
    }

    private void handleDataResponse(JSONObject response) {
        try {
            JSONArray jsonArray = response.getJSONArray(KEY_COUNTRY);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String country = jsonObject.getString(KEY_NAME);
                mCountryList.add(new Country(id, country));
                mCitizenshipList.add(new Citizenship(id, country));
            }
            mCountryArrayAdapter.notifyDataSetChanged();
            mCitizenshipArrayAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
