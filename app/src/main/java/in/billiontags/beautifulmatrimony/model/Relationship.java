package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 03/07/17
 */

public class Relationship {

    private int mId;
    private String mRelationship;

    public Relationship(int id, String relationship) {
        mId = id;
        mRelationship = relationship;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getRelationship() {
        return mRelationship;
    }

    public void setRelationship(String relationship) {
        mRelationship = relationship;
    }

    @Override
    public String toString() {
        return mRelationship;
    }
}
