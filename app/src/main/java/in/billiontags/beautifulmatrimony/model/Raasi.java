package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 06/07/17
 */

public class Raasi {

    private int mId;
    private String mRaasi;

    public Raasi(int id, String raasi) {
        mId = id;
        mRaasi = raasi;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getRaasi() {
        return mRaasi;
    }

    public void setRaasi(String raasi) {
        mRaasi = raasi;
    }

    @Override
    public String toString() {
        return mRaasi;
    }
}
