package in.billiontags.beautifulmatrimony.model;

import java.util.List;

/**
 * Created by Bobby on 03/07/17
 */

public class Education {

    private int mId;
    private String mEducation;
    private List<Branch> mBranchList;

    public Education(int id, String education) {
        mId = id;
        mEducation = education;
    }

    public Education(int id, String education, List<Branch> branchList) {
        mId = id;
        mEducation = education;
        mBranchList = branchList;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getEducation() {
        return mEducation;
    }

    public void setEducation(String education) {
        mEducation = education;
    }

    public List<Branch> getBranchList() {
        return mBranchList;
    }

    public void setBranchList(List<Branch> branchList) {
        mBranchList = branchList;
    }

    @Override
    public String toString() {
        return mEducation;
    }
}
