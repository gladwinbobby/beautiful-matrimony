package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 03-05-2017
 */

public class MessageItem {

    private int mId;
    private String mMessage, mImage, mDate;
    private boolean mDisplayImage;

    public MessageItem(int id, String message, String image, String date,
            boolean displayImage) {
        mId = id;
        mMessage = message;
        mImage = image;
        mDate = date;
        mDisplayImage = displayImage;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public boolean isDisplayImage() {
        return mDisplayImage;
    }

    public void setDisplayImage(boolean displayImage) {
        mDisplayImage = displayImage;
    }
}
