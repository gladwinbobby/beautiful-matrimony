package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 04/07/17
 */

public class Country {

    private int mId;
    private String mCountry;

    public Country(int id, String country) {
        mId = id;
        mCountry = country;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    @Override
    public String toString() {
        return mCountry;
    }
}
