package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 06/07/17
 */

public class BodyType {

    private int mId;
    private String mBodyType;

    public BodyType(int id, String bodyType) {
        mId = id;
        mBodyType = bodyType;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getBodyType() {
        return mBodyType;
    }

    public void setBodyType(String bodyType) {
        mBodyType = bodyType;
    }

    @Override
    public String toString() {
        return mBodyType;
    }
}
