package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 04/07/17
 */

public class State {

    private int mId;
    private String mState;

    public State(int id, String state) {
        mId = id;
        mState = state;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    @Override
    public String toString() {
        return mState;
    }
}
