package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 04/07/17
 */

public class Dosham {

    private int mId;
    private String mDosham;
    private boolean mSelected;

    public Dosham(int id, String dosham, boolean selected) {
        mId = id;
        mDosham = dosham;
        mSelected = selected;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getDosham() {
        return mDosham;
    }

    public void setDosham(String dosham) {
        mDosham = dosham;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }

    @Override
    public String toString() {
        return mDosham;
    }
}
