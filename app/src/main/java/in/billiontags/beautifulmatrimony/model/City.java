package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 04/07/17
 */

public class City {

    private int mId;
    private String mCity;

    public City(int id, String city) {
        mId = id;
        mCity = city;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    @Override
    public String toString() {
        return mCity;
    }
}
