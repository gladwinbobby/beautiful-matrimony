package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 02-05-2017
 */

public class SuggestionItem {

    private int mId;
    private String mIdentity, mImage;

    public SuggestionItem(int id, String identity, String image) {
        mId = id;
        mIdentity = identity;
        mImage = image;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getIdentity() {
        return mIdentity;
    }

    public void setIdentity(String identity) {
        mIdentity = identity;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }
}
