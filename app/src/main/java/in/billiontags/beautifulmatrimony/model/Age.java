package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 06/07/17
 */

public class Age {

    private int mId;
    private String mAge;

    public Age(int id, String age) {
        mId = id;
        mAge = age;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getAge() {
        return mAge;
    }

    public void setAge(String age) {
        mAge = age;
    }

    @Override
    public String toString() {
        return mAge;
    }
}
