package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 06/07/17
 */

public class FatherStatus {

    private int mId;
    private String mFatherStatus;

    public FatherStatus(int id, String fatherStatus) {
        mId = id;
        mFatherStatus = fatherStatus;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getFatherStatus() {
        return mFatherStatus;
    }

    public void setFatherStatus(String fatherStatus) {
        mFatherStatus = fatherStatus;
    }

    @Override
    public String toString() {
        return mFatherStatus;
    }
}
