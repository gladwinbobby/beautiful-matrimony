package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 03/07/17
 */

public class Religion {

    private int mId;
    private String mReligion;

    public Religion(int id, String religion) {
        mId = id;
        mReligion = religion;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getReligion() {
        return mReligion;
    }

    public void setReligion(String religion) {
        mReligion = religion;
    }

    @Override
    public String toString() {
        return mReligion;
    }
}
