package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 04/07/17
 */

public class SubCaste {

    private int mId;
    private String mSubCaste;

    public SubCaste(int id, String subCaste) {
        mId = id;
        mSubCaste = subCaste;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getSubCaste() {
        return mSubCaste;
    }

    public void setSubCaste(String subCaste) {
        mSubCaste = subCaste;
    }

    @Override
    public String toString() {
        return mSubCaste;
    }
}
