package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 05/07/17
 */

public class FamilyValue {

    private int mId;
    private String mFamilyValue;

    public FamilyValue(int id, String familyValue) {
        mId = id;
        mFamilyValue = familyValue;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getFamilyValue() {
        return mFamilyValue;
    }

    public void setFamilyValue(String familyValue) {
        mFamilyValue = familyValue;
    }

    @Override
    public String toString() {
        return mFamilyValue;
    }
}
