package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 04/07/17
 */

public class Caste {

    private int mId;
    private String mCaste;

    public Caste(int id, String caste) {
        mId = id;
        mCaste = caste;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getCaste() {
        return mCaste;
    }

    public void setCaste(String caste) {
        mCaste = caste;
    }

    @Override
    public String toString() {
        return mCaste;
    }
}
