package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 10/07/17
 */

public class Matrimony {

    private int mId, mUid;
    private String mFirstName, mLastName, mProfilePic, mGender, mStreet, mArea, mCity, mState,
            mCountry, mZipcode;
    private boolean mShortlisted, mIgnored;

    public Matrimony(int id, int uid, String firstName, String lastName, String profilePic,
            String gender, String street, String area, String city, String state,
            String country, String zipcode, boolean shortlisted, boolean ignored) {
        mId = id;
        mUid = uid;
        mFirstName = firstName;
        mLastName = lastName;
        mProfilePic = profilePic;
        mGender = gender;
        mStreet = street;
        mArea = area;
        mCity = city;
        mState = state;
        mCountry = country;
        mZipcode = zipcode;
        mShortlisted = shortlisted;
        mIgnored = ignored;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getUid() {
        return mUid;
    }

    public void setUid(int uid) {
        mUid = uid;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getProfilePic() {
        return mProfilePic;
    }

    public void setProfilePic(String profilePic) {
        mProfilePic = profilePic;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String street) {
        mStreet = street;
    }

    public String getArea() {
        return mArea;
    }

    public void setArea(String area) {
        mArea = area;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getZipcode() {
        return mZipcode;
    }

    public void setZipcode(String zipcode) {
        mZipcode = zipcode;
    }

    public boolean isShortlisted() {
        return mShortlisted;
    }

    public void setShortlisted(boolean shortlisted) {
        mShortlisted = shortlisted;
    }

    public boolean isIgnored() {
        return mIgnored;
    }

    public void setIgnored(boolean ignored) {
        mIgnored = ignored;
    }
}
