package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 06/07/17
 */

public class Sport {

    private int mId;
    private String mSport;
    private boolean mSelected;

    public Sport(int id, String sport, boolean selected) {
        mId = id;
        mSport = sport;
        mSelected = selected;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getSport() {
        return mSport;
    }

    public void setSport(String sport) {
        mSport = sport;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }

    @Override
    public String toString() {
        return mSport;
    }
}
