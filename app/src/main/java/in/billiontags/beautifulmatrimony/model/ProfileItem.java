package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 02-05-2017
 */

public class ProfileItem {

    private int mId, mMatch;
    private String mIdentity, mLocation, mImage;

    public ProfileItem(int id, int match, String identity, String location, String image) {
        mId = id;
        mMatch = match;
        mIdentity = identity;
        mLocation = location;
        mImage = image;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getMatch() {
        return mMatch;
    }

    public void setMatch(int match) {
        mMatch = match;
    }

    public String getIdentity() {
        return mIdentity;
    }

    public void setIdentity(String identity) {
        mIdentity = identity;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }
}
