package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 03-05-2017
 */

public class ThumbnailItem {

    private int mId;
    private String mThumbnail;
    private boolean mSelected;

    public ThumbnailItem(int id, String thumbnail, boolean selected) {
        mId = id;
        mThumbnail = thumbnail;
        mSelected = selected;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(String thumbnail) {
        mThumbnail = thumbnail;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }
}
