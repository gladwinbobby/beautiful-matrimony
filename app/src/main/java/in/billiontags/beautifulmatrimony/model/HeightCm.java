package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 05/07/17
 */

public class HeightCm {

    private float mHeight;
    private String mHeightCm;

    public HeightCm(float height, String heightCm) {
        mHeight = height;
        mHeightCm = heightCm;
    }

    public float getHeight() {
        return mHeight;
    }

    public void setHeight(float height) {
        mHeight = height;
    }

    public String getHeightCm() {
        return mHeightCm;
    }

    public void setHeightCm(String heightCm) {
        mHeightCm = heightCm;
    }

    @Override
    public String toString() {
        return mHeightCm;
    }
}
