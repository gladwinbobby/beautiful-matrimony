package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 03-05-2017
 */

public class ResponseItem {

    private int mId;
    private String mMessage, mDate;

    public ResponseItem(int id, String message, String date) {
        mId = id;
        mMessage = message;
        mDate = date;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }
}
