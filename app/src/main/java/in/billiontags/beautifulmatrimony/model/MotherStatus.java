package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 06/07/17
 */

public class MotherStatus {

    private int mId;
    private String mMotherStatus;

    public MotherStatus(int id, String motherStatus) {
        mId = id;
        mMotherStatus = motherStatus;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getMotherStatus() {
        return mMotherStatus;
    }

    public void setMotherStatus(String motherStatus) {
        mMotherStatus = motherStatus;
    }

    @Override
    public String toString() {
        return mMotherStatus;
    }
}
