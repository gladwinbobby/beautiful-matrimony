package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 05/07/17
 */

public class HeightFt {

    private float mHeight;
    private String mHeightFt;

    public HeightFt(float height, String heightFt) {
        mHeight = height;
        mHeightFt = heightFt;
    }

    public float getHeight() {
        return mHeight;
    }

    public void setHeight(float height) {
        mHeight = height;
    }

    public String getHeightFt() {
        return mHeightFt;
    }

    public void setHeightFt(String heightFt) {
        mHeightFt = heightFt;
    }

    @Override
    public String toString() {
        return mHeightFt;
    }
}
