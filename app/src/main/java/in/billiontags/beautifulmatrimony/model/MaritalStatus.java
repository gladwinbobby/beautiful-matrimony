package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 05/07/17
 */

public class MaritalStatus {

    private int mId;
    private String mMaritalStatus;

    public MaritalStatus(int id, String maritalStatus) {
        mId = id;
        mMaritalStatus = maritalStatus;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getMaritalStatus() {
        return mMaritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        mMaritalStatus = maritalStatus;
    }

    @Override
    public String toString() {
        return mMaritalStatus;
    }
}
