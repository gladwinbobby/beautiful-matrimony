package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 06/07/17
 */

public class Weight {

    private int mId;
    private String mWeight;

    public Weight(int id, String weight) {
        mId = id;
        mWeight = weight;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getWeight() {
        return mWeight;
    }

    public void setWeight(String weight) {
        mWeight = weight;
    }

    @Override
    public String toString() {
        return mWeight;
    }
}
