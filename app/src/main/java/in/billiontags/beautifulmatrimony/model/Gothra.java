package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 04/07/17
 */

public class Gothra {

    private int mId;
    private String mGothra;

    public Gothra(int id, String gothra) {
        mId = id;
        mGothra = gothra;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getGothra() {
        return mGothra;
    }

    public void setGothra(String gothra) {
        mGothra = gothra;
    }

    @Override
    public String toString() {
        return mGothra;
    }
}
