package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 06/07/17
 */

public class Star {

    private int mId;
    private String mStar;

    public Star(int id, String star) {
        mId = id;
        mStar = star;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getStar() {
        return mStar;
    }

    public void setStar(String star) {
        mStar = star;
    }

    @Override
    public String toString() {
        return mStar;
    }
}
