package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 02-05-2017
 */

public class ChatItem {

    private int mId;
    private String mIdentity, mLocation, mImage;

    public ChatItem(int id, String identity, String location, String image) {
        mId = id;
        mIdentity = identity;
        mLocation = location;
        mImage = image;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getIdentity() {
        return mIdentity;
    }

    public void setIdentity(String identity) {
        mIdentity = identity;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }
}
