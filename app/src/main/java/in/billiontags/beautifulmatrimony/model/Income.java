package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 05/07/17
 */

public class Income {

    private int mId;
    private String mIncome;

    public Income(int id, String income) {
        mId = id;
        mIncome = income;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getIncome() {
        return mIncome;
    }

    public void setIncome(String income) {
        mIncome = income;
    }

    @Override
    public String toString() {
        return mIncome;
    }
}
