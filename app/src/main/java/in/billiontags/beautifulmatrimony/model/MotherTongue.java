package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 03/07/17
 */

public class MotherTongue {

    private int mId;
    private String mMotherTongue;

    public MotherTongue(int id, String motherTongue) {
        mId = id;
        mMotherTongue = motherTongue;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getMotherTongue() {
        return mMotherTongue;
    }

    public void setMotherTongue(String motherTongue) {
        mMotherTongue = motherTongue;
    }

    @Override
    public String toString() {
        return mMotherTongue;
    }
}
