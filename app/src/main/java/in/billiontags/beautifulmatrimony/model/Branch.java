package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 03/07/17
 */

public class Branch {

    private int mId;
    private String mBranch;

    public Branch(int id, String branch) {
        mId = id;
        mBranch = branch;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getBranch() {
        return mBranch;
    }

    public void setBranch(String branch) {
        mBranch = branch;
    }

    @Override
    public String toString() {
        return mBranch;
    }
}
