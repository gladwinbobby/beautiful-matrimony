package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 07/07/17
 */

public class Citizenship {

    private int mId;
    private String mCitizenship;

    public Citizenship(int id, String citizenship) {
        mId = id;
        mCitizenship = citizenship;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getCitizenship() {
        return mCitizenship;
    }

    public void setCitizenship(String citizenship) {
        mCitizenship = citizenship;
    }

    @Override
    public String toString() {
        return mCitizenship;
    }
}
