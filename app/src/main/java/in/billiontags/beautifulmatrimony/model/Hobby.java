package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 06/07/17
 */

public class Hobby {

    private int mId;
    private String mHobby;
    private boolean mSelected;

    public Hobby(int id, String hobby, boolean selected) {
        mId = id;
        mHobby = hobby;
        mSelected = selected;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getHobby() {
        return mHobby;
    }

    public void setHobby(String hobby) {
        mHobby = hobby;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }

    @Override
    public String toString() {
        return mHobby;
    }
}
