package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 06/07/17
 */

public class BloodType {

    private int mId;
    private String mBloodType;

    public BloodType(int id, String bloodType) {
        mId = id;
        mBloodType = bloodType;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getBloodType() {
        return mBloodType;
    }

    public void setBloodType(String bloodType) {
        mBloodType = bloodType;
    }

    @Override
    public String toString() {
        return mBloodType;
    }
}
