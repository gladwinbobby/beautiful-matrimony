package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 05/07/17
 */

public class Currency {

    private int mId;
    private String mCurrency;

    public Currency(int id, String currency) {
        mId = id;
        mCurrency = currency;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        mCurrency = currency;
    }

    @Override
    public String toString() {
        return mCurrency;
    }
}
