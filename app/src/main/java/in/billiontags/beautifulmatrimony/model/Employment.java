package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 05/07/17
 */

public class Employment {

    private int mId;
    private String mEmployment;

    public Employment(int id, String employment) {
        mId = id;
        mEmployment = employment;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getEmployment() {
        return mEmployment;
    }

    public void setEmployment(String employment) {
        mEmployment = employment;
    }

    @Override
    public String toString() {
        return mEmployment;
    }
}
