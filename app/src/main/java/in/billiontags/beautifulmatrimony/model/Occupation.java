package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 05/07/17
 */

public class Occupation {

    private int mId;
    private String mOccupation;

    public Occupation(int id, String occupation) {
        mId = id;
        mOccupation = occupation;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getOccupation() {
        return mOccupation;
    }

    public void setOccupation(String occupation) {
        mOccupation = occupation;
    }

    @Override
    public String toString() {
        return mOccupation;
    }
}
