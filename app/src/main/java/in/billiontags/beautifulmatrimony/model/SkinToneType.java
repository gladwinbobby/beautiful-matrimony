package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 06/07/17
 */

public class SkinToneType {

    private int mId;
    private String mSkinToneType;

    public SkinToneType(int id, String skinToneType) {
        mId = id;
        mSkinToneType = skinToneType;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getSkinToneType() {
        return mSkinToneType;
    }

    public void setSkinToneType(String skinToneType) {
        mSkinToneType = skinToneType;
    }

    @Override
    public String toString() {
        return mSkinToneType;
    }
}
