package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 05/07/17
 */

public class FamilyStatus {

    private int mId;
    private String mFamilyStatus;

    public FamilyStatus(int id, String familyStatus) {
        mId = id;
        mFamilyStatus = familyStatus;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getFamilyStatus() {
        return mFamilyStatus;
    }

    public void setFamilyStatus(String familyStatus) {
        mFamilyStatus = familyStatus;
    }

    @Override
    public String toString() {
        return mFamilyStatus;
    }
}
