package in.billiontags.beautifulmatrimony.model;

/**
 * Created by Bobby on 06/07/17
 */

public class Music {

    private int mId;
    private String mMusic;
    private boolean mSelected;

    public Music(int id, String music, boolean selected) {
        mId = id;
        mMusic = music;
        mSelected = selected;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getMusic() {
        return mMusic;
    }

    public void setMusic(String music) {
        mMusic = music;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }

    @Override
    public String toString() {
        return mMusic;
    }
}
