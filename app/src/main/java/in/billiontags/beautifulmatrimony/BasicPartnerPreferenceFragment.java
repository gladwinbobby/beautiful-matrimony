package in.billiontags.beautifulmatrimony;


import static in.billiontags.beautifulmatrimony.helper.Utils.getAgeList;
import static in.billiontags.beautifulmatrimony.helper.Utils.getHeightFtList;
import static in.billiontags.beautifulmatrimony.helper.Utils.getMaritalStatusList;
import static in.billiontags.beautifulmatrimony.helper.Utils.getWeightList;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;
import in.billiontags.beautifulmatrimony.model.Age;
import in.billiontags.beautifulmatrimony.model.HeightFt;
import in.billiontags.beautifulmatrimony.model.MaritalStatus;
import in.billiontags.beautifulmatrimony.model.Weight;


/**
 * A simple {@link Fragment} subclass.
 */
public class BasicPartnerPreferenceFragment extends Fragment implements View.OnClickListener,
        AdapterView.OnItemSelectedListener {

    private View mRootView;
    private Context mContext;
    private SearchableSpinner mSpinnerAgeFrom, mSpinnerAgeTo, mSpinnerHeightFrom, mSpinnerHeightTo,
            mSpinnerWeightFrom, mSpinnerWeightTo, mSpinnerMaritalStatus;
    private RadioGroup mGroupPhysicalStatus, mGroupEatingHabit, mGroupDrinkingHabit,
            mGroupSmokingHabit;
    private Button mButtonSkip, mButtonContinue;
    private List<Age> mAgeFromList, mAgeToList;
    private List<HeightFt> mHeightFtFromList, mHeightFtToList;
    private List<Weight> mWeightFromList, mWeightToList;
    private List<MaritalStatus> mMaritalStatusList;
    private ArrayAdapter<Age> mAgeFromArrayAdapter, mAgeToArrayAdapter;
    private ArrayAdapter<HeightFt> mHeightFtFromArrayAdapter, mHeightFtToArrayAdapter;
    private ArrayAdapter<Weight> mWeightFromArrayAdapter, mWeightToArrayAdapter;
    private ArrayAdapter<MaritalStatus> mMaritalStatusArrayAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;

    public BasicPartnerPreferenceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_basic_partner_preference, container, false);
        initObjects();
        initCallbacks();
        initSpinners();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSkip) {
            mCallback.launchNextScreen();
        } else if (v == mButtonContinue) {
            mCallback.launchNextScreen();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void initObjects() {
        mSpinnerAgeFrom = (SearchableSpinner) mRootView.findViewById(R.id.spin_age_from);
        mSpinnerAgeTo = (SearchableSpinner) mRootView.findViewById(R.id.spin_age_to);
        mSpinnerHeightFrom = (SearchableSpinner) mRootView.findViewById(R.id.spin_height_from);
        mSpinnerHeightTo = (SearchableSpinner) mRootView.findViewById(R.id.spin_height_to);
        mSpinnerWeightFrom = (SearchableSpinner) mRootView.findViewById(R.id.spin_weight_from);
        mSpinnerWeightTo = (SearchableSpinner) mRootView.findViewById(R.id.spin_weight_to);
        mSpinnerMaritalStatus = (SearchableSpinner) mRootView.findViewById(
                R.id.spin_marital_status);
        mGroupPhysicalStatus = (RadioGroup) mRootView.findViewById(R.id.grp_physical_status);
        mGroupEatingHabit = (RadioGroup) mRootView.findViewById(R.id.grp_eating_habit);
        mGroupDrinkingHabit = (RadioGroup) mRootView.findViewById(R.id.grp_drinking_habit);
        mGroupSmokingHabit = (RadioGroup) mRootView.findViewById(R.id.grp_smoking_habit);
        mButtonSkip = (Button) mRootView.findViewById(R.id.btn_skip);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);

        mContext = getActivity();
        mAgeFromList = new ArrayList<>();
        mAgeToList = new ArrayList<>();
        mHeightFtFromList = new ArrayList<>();
        mHeightFtToList = new ArrayList<>();
        mWeightFromList = new ArrayList<>();
        mWeightToList = new ArrayList<>();
        mMaritalStatusList = new ArrayList<>();
        mAgeFromArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mAgeFromList);
        mAgeToArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text, mAgeToList);
        mHeightFtFromArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mHeightFtFromList);
        mHeightFtToArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mHeightFtToList);
        mWeightFromArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mWeightFromList);
        mWeightToArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mWeightToList);
        mMaritalStatusArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mMaritalStatusList);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mSpinnerAgeFrom.setOnItemSelectedListener(this);
        mSpinnerAgeTo.setOnItemSelectedListener(this);
        mSpinnerHeightFrom.setOnItemSelectedListener(this);
        mSpinnerHeightTo.setOnItemSelectedListener(this);
        mSpinnerWeightFrom.setOnItemSelectedListener(this);
        mSpinnerWeightTo.setOnItemSelectedListener(this);
        mButtonSkip.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
    }

    private void initSpinners() {
        mSpinnerAgeFrom.setTitle("Select Min Age");
        mSpinnerAgeTo.setTitle("Select Max Age");
        mSpinnerHeightFrom.setTitle("Select Min Height");
        mSpinnerHeightTo.setTitle("Select Max Height");
        mSpinnerWeightFrom.setTitle("Select Min Weight");
        mSpinnerWeightTo.setTitle("Select Max Weight");
        mSpinnerMaritalStatus.setTitle("Select Marital Status");

        mAgeFromList.addAll(getAgeList());
        mAgeToList.addAll(getAgeList());
        mHeightFtFromList.addAll(getHeightFtList());
        mHeightFtToList.addAll(getHeightFtList());
        mWeightFromList.addAll(getWeightList());
        mWeightToList.addAll(getWeightList());
        mMaritalStatusList.addAll(getMaritalStatusList());

        mAgeFromArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerAgeFrom.setAdapter(mAgeFromArrayAdapter);
        mAgeToArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerAgeTo.setAdapter(mAgeToArrayAdapter);
        mHeightFtFromArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerHeightFrom.setAdapter(mHeightFtFromArrayAdapter);
        mHeightFtToArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerHeightTo.setAdapter(mHeightFtToArrayAdapter);
        mWeightFromArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerWeightFrom.setAdapter(mWeightFromArrayAdapter);
        mWeightToArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerWeightTo.setAdapter(mWeightToArrayAdapter);
        mMaritalStatusArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerMaritalStatus.setAdapter(mMaritalStatusArrayAdapter);
    }
}
