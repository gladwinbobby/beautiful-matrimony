package in.billiontags.beautifulmatrimony;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;


/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment implements View.OnClickListener {

    private View mRootView;
    private Context mContext;
    private ProgressBar mProgressBar;
    private EditText mEditTextAboutMe;
    private Button mButtonSkip, mButtonContinue, mButtonRetry;
    private LinearLayout mLayoutAbout, mLayoutFooter, mLayoutOops;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_about, container, false);
        initObjects();
        initCallbacks();
        displayLoading();
        displayAbout();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSkip) {
            mCallback.launchNextScreen();
        } else if (v == mButtonContinue) {
            mCallback.launchNextScreen();
        } else if (v == mButtonRetry) {
            displayLoading();
            displayAbout();
        }
    }

    private void initObjects() {
        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progress);
        mEditTextAboutMe = (EditText) mRootView.findViewById(R.id.input_about_me);
        mButtonSkip = (Button) mRootView.findViewById(R.id.btn_skip);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);
        mButtonRetry = (Button) mRootView.findViewById(R.id.btn_retry);
        mLayoutFooter = (LinearLayout) mRootView.findViewById(R.id.footer);
        mLayoutAbout = (LinearLayout) mRootView.findViewById(R.id.about);
        mLayoutOops = (LinearLayout) mRootView.findViewById(R.id.oops);

        mContext = getActivity();
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonSkip.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
        mButtonRetry.setOnClickListener(this);
    }

    private void displayAbout() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutFooter.setVisibility(View.VISIBLE);
        mLayoutAbout.setVisibility(View.VISIBLE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
        mLayoutFooter.setVisibility(View.GONE);
        mLayoutAbout.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.GONE);
    }

    private void displayOops() {
        mProgressBar.setVisibility(View.GONE);
        mLayoutFooter.setVisibility(View.GONE);
        mLayoutAbout.setVisibility(View.GONE);
        mLayoutOops.setVisibility(View.VISIBLE);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
