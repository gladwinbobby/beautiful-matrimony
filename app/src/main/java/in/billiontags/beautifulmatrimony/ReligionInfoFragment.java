package in.billiontags.beautifulmatrimony;


import static in.billiontags.beautifulmatrimony.app.Api.DATA_PHASE_5;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_ID;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_NAME;
import static in.billiontags.beautifulmatrimony.app.Api.KEY_STAR;
import static in.billiontags.beautifulmatrimony.app.Api.RAASI;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.app.AppController;
import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.app.VolleyErrorHandler;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;
import in.billiontags.beautifulmatrimony.model.Raasi;
import in.billiontags.beautifulmatrimony.model.Star;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReligionInfoFragment extends Fragment implements View.OnClickListener,
        AdapterView.OnItemSelectedListener {

    private View mRootView;
    private Context mContext;
    private SearchableSpinner mSpinnerStar, mSpinnerRaasi;
    private Button mButtonSkip, mButtonContinue;
    private List<Star> mStarList;
    private List<Raasi> mRaasiList;
    private ArrayAdapter<Star> mStarArrayAdapter;
    private ArrayAdapter<Raasi> mRaasiArrayAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;

    public ReligionInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_religion_info, container, false);
        initObjects();
        initCallbacks();
        initSpinners();
        showProgressDialog("Loading..");
        getData();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSkip) {
            mCallback.launchNextScreen();
        } else if (v == mButtonContinue) {
            mCallback.launchNextScreen();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == mSpinnerStar) {
            getRaasi(mStarList.get(position).getId());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void initObjects() {
        mSpinnerStar = (SearchableSpinner) mRootView.findViewById(R.id.spin_star);
        mSpinnerRaasi = (SearchableSpinner) mRootView.findViewById(R.id.spin_raasi);
        mButtonSkip = (Button) mRootView.findViewById(R.id.btn_skip);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);

        mContext = getActivity();
        mStarList = new ArrayList<>();
        mRaasiList = new ArrayList<>();
        mStarArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text, mStarList);
        mRaasiArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text, mRaasiList);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mSpinnerStar.setOnItemSelectedListener(this);
        mButtonSkip.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
    }

    private void initSpinners() {
        mSpinnerStar.setTitle("Select Star");
        mSpinnerRaasi.setTitle("Select Raasi");
        mStarArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerStar.setAdapter(mStarArrayAdapter);
        mRaasiArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerRaasi.setAdapter(mRaasiArrayAdapter);
    }

    private void clearData() {
        mStarList.clear();
        mRaasiList.clear();
    }

    private void getData() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(DATA_PHASE_5, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgressDialog();
                        clearData();
                        handleDataResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "data_phase5");
    }

    private void handleDataResponse(JSONObject response) {
        try {
            JSONArray jsonArray = response.getJSONArray(KEY_STAR);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String star = jsonObject.getString(KEY_NAME);
                mStarList.add(new Star(id, star));
            }
            mStarArrayAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getRaasi(int id) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(RAASI + id + "/",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        mRaasiList.clear();
                        handleRaasiResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonArrayRequest, "raasi");
    }

    private void handleRaasiResponse(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jsonObject = response.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String raasi = jsonObject.getString(KEY_NAME);
                mRaasiList.add(new Raasi(id, raasi));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mRaasiArrayAdapter.notifyDataSetChanged();
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
