package in.billiontags.beautifulmatrimony.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckedTextView;

import in.billiontags.beautifulmatrimony.R;
import in.billiontags.beautifulmatrimony.callback.HobbiesInterestsCallback;

/**
 * Created by Bobby on 04/07/17
 */

public class HobbiesInterestsHolder extends RecyclerView.ViewHolder implements
        View.OnClickListener {

    public CheckedTextView mTextViewData;
    private HobbiesInterestsCallback mCallback;

    public HobbiesInterestsHolder(View itemView, HobbiesInterestsCallback callback) {
        super(itemView);
        mTextViewData = (CheckedTextView) itemView.findViewById(R.id.txt_data);
        mCallback = callback;
        mTextViewData.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mTextViewData) {
            mCallback.onHobbiesInterestsClick(getAdapterPosition());
        }
    }
}
