package in.billiontags.beautifulmatrimony.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import in.billiontags.beautifulmatrimony.R;

/**
 * Created by Bobby on 02-05-2017
 */

public class ProgressHolder extends RecyclerView.ViewHolder {

    private ProgressBar mProgressBar;

    public ProgressHolder(View itemView) {
        super(itemView);
        mProgressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
    }

    public ProgressBar getProgressBar() {
        return mProgressBar;
    }
}
