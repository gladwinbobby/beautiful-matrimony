package in.billiontags.beautifulmatrimony.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import de.hdodenhof.circleimageview.CircleImageView;
import in.billiontags.beautifulmatrimony.R;

/**
 * Created by Bobby on 03-05-2017
 */

public class ThumbnailHolder extends RecyclerView.ViewHolder {

    private CircleImageView mImageViewThumbnail;

    public ThumbnailHolder(View itemView) {
        super(itemView);
        mImageViewThumbnail = (CircleImageView) itemView.findViewById(R.id.img_thumbnail);
    }

    public CircleImageView getImageViewThumbnail() {
        return mImageViewThumbnail;
    }
}
