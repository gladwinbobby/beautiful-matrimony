package in.billiontags.beautifulmatrimony.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.billiontags.beautifulmatrimony.R;

/**
 * Created by Bobby on 02-05-2017
 */

public class SuggestionHolder extends RecyclerView.ViewHolder {

    private ImageView mImageViewProfile;
    private TextView mTextViewIdentity;

    public SuggestionHolder(View itemView) {
        super(itemView);
        mImageViewProfile = (ImageView) itemView.findViewById(R.id.img_profile);
        mTextViewIdentity = (TextView) itemView.findViewById(R.id.txt_identity);
    }

    public ImageView getImageViewProfile() {
        return mImageViewProfile;
    }

    public TextView getTextViewIdentity() {
        return mTextViewIdentity;
    }
}
