package in.billiontags.beautifulmatrimony.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.billiontags.beautifulmatrimony.R;

/**
 * Created by Bobby on 03-05-2017
 */

public class ResponseHolder extends RecyclerView.ViewHolder {

    private TextView mTextViewMessage;

    public ResponseHolder(View itemView) {
        super(itemView);
        mTextViewMessage = (TextView) itemView.findViewById(R.id.txt_message);
    }

    public TextView getTextViewMessage() {
        return mTextViewMessage;
    }
}
