package in.billiontags.beautifulmatrimony.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import in.billiontags.beautifulmatrimony.R;

/**
 * Created by Bobby on 03-05-2017
 */

public class MessageHolder extends RecyclerView.ViewHolder {

    private CircleImageView mImageViewProfile;
    private TextView mTextViewMessage;

    public MessageHolder(View itemView) {
        super(itemView);
        mImageViewProfile = (CircleImageView) itemView.findViewById(R.id.img_profile);
        mTextViewMessage = (TextView) itemView.findViewById(R.id.txt_message);
    }

    public CircleImageView getImageViewProfile() {
        return mImageViewProfile;
    }

    public TextView getTextViewMessage() {
        return mTextViewMessage;
    }
}
