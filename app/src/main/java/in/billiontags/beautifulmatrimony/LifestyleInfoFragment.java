package in.billiontags.beautifulmatrimony;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;


/**
 * A simple {@link Fragment} subclass.
 */
public class LifestyleInfoFragment extends Fragment implements View.OnClickListener {

    private View mRootView;
    private Context mContext;
    private RadioGroup mGroupEatingHabit, mGroupDrinkingHabit, mGroupSmokingHabit;
    private Button mButtonSkip, mButtonContinue;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;

    public LifestyleInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_lifestyle_info, container, false);
        initObjects();
        initCallbacks();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSkip) {
            mCallback.launchNextScreen();
        } else if (v == mButtonContinue) {
            mCallback.launchNextScreen();
        }
    }

    private void initObjects() {
        mGroupEatingHabit = (RadioGroup) mRootView.findViewById(R.id.grp_eating_habit);
        mGroupDrinkingHabit = (RadioGroup) mRootView.findViewById(R.id.grp_drinking_habit);
        mGroupSmokingHabit = (RadioGroup) mRootView.findViewById(R.id.grp_smoking_habit);
        mButtonSkip = (Button) mRootView.findViewById(R.id.btn_skip);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);

        mContext = getActivity();
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonSkip.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
    }
}
