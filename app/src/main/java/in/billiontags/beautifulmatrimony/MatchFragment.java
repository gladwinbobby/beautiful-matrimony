package in.billiontags.beautifulmatrimony;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.adapter.MatrimonyAdapter;
import in.billiontags.beautifulmatrimony.model.ProfileItem;
import link.fls.swipestack.SwipeStack;


/**
 * A simple {@link Fragment} subclass.
 */
public class MatchFragment extends Fragment
        implements TabLayout.OnTabSelectedListener, SwipeStack.SwipeStackListener {

    private Context mContext;
    private View mRootView;
    private TabLayout mTabLayout;
    private SwipeStack mSwipeStack;
    private List<ProfileItem> mProfileItemList;
    private MatrimonyAdapter mAdapter;

    public MatchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_match, container, false);
        initObjects();
        initCallbacks();
        initTabs();
        initAdapterView();
        setTabsFont();
        populateData();
        return mRootView;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onViewSwipedToLeft(int position) {
        Toast.makeText(mContext, "Hate ya", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onViewSwipedToRight(int position) {
        Toast.makeText(mContext, "Love ya", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStackEmpty() {
        populateData();
    }

    private void initObjects() {
        mTabLayout = (TabLayout) mRootView.findViewById(R.id.tab);
        mSwipeStack = (SwipeStack) mRootView.findViewById(R.id.profile);

        mContext = getActivity();
        mProfileItemList = new ArrayList<>();
//        mAdapter = new MatrimonyAdapter(mContext, mProfileItemList);
    }

    private void initCallbacks() {
        mTabLayout.addOnTabSelectedListener(this);
        mSwipeStack.setListener(this);
    }

    private void initTabs() {
        mTabLayout.addTab(mTabLayout.newTab().setText("New Matches"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Yet To Be Viewed"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Shortlisted Matches"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Mutual Matches"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Members Looking For Me"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Premium Members"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Daily Matches"));
    }

    private void initAdapterView() {
        mSwipeStack.setAdapter(mAdapter);
    }

    private void setTabsFont() {
        ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(
                            Typeface.createFromAsset(mContext.getAssets(), "fonts/coves_light.otf"),
                            Typeface.NORMAL);
                    ((TextView) tabViewChild).setTextSize(
                            getResources().getDimension(R.dimen.small_txt));
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }

    private void populateData() {
        mProfileItemList.add(
                new ProfileItem(3, 95, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                        "http://i.imgur.com/xCM0CKx.jpg"));
        mProfileItemList.add(
                new ProfileItem(4, 96, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                        "http://i.imgur.com/s5fGQkY.jpg"));
        mProfileItemList.add(
                new ProfileItem(5, 97, "xxx xxx, 29 years old", "Saidapet, Tamil Nadu",
                        "http://i.imgur.com/TBhAZ3U.jpg"));
        mProfileItemList.add(
                new ProfileItem(1, 93, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                        "http://i.imgur.com/7DtMVjU.jpg"));
        mProfileItemList.add(
                new ProfileItem(2, 94, "xxx xxx, 27 years old", "Saidapet, Tamil Nadu",
                        "http://i.imgur.com/CEGlxcp.jpg"));
        mAdapter.notifyDataSetChanged();
    }
}
