package in.billiontags.beautifulmatrimony;


import static in.billiontags.beautifulmatrimony.helper.Utils.getBodyTypeList;
import static in.billiontags.beautifulmatrimony.helper.Utils.getSkinToneTypeList;
import static in.billiontags.beautifulmatrimony.helper.Utils.getWeightList;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.beautifulmatrimony.app.MyPreference;
import in.billiontags.beautifulmatrimony.callback.RegisterCallback;
import in.billiontags.beautifulmatrimony.model.BodyType;
import in.billiontags.beautifulmatrimony.model.SkinToneType;
import in.billiontags.beautifulmatrimony.model.Weight;


/**
 * A simple {@link Fragment} subclass.
 */
public class BasicInformationFragment extends Fragment implements View.OnClickListener {

    private View mRootView;
    private Context mContext;
    private SearchableSpinner mSpinnerBodyType, mSpinnerSkinToneType, mSpinnerWeight;
    private Button mButtonSkip, mButtonContinue;
    private List<BodyType> mBodyTypeList;
    private List<SkinToneType> mSkinToneTypeList;
    private List<Weight> mWeightList;
    private ArrayAdapter<BodyType> mBodyTypeArrayAdapter;
    private ArrayAdapter<SkinToneType> mSkinToneTypeArrayAdapter;
    private ArrayAdapter<Weight> mWeightArrayAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private RegisterCallback mCallback;

    public BasicInformationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_basic_information, container, false);
        initObjects();
        initCallbacks();
        initSpinners();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterActivity) {
            mCallback = (RegisterCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSkip) {
            mCallback.launchNextScreen();
        } else if (v == mButtonContinue) {
            mCallback.launchNextScreen();
        }
    }

    private void initObjects() {
        mSpinnerBodyType = (SearchableSpinner) mRootView.findViewById(R.id.spin_body_type);
        mSpinnerSkinToneType = (SearchableSpinner) mRootView.findViewById(R.id.spin_skin_tone_type);
        mSpinnerWeight = (SearchableSpinner) mRootView.findViewById(R.id.spin_weight);
        mButtonSkip = (Button) mRootView.findViewById(R.id.btn_skip);
        mButtonContinue = (Button) mRootView.findViewById(R.id.btn_continue);

        mContext = getActivity();
        mBodyTypeList = new ArrayList<>();
        mSkinToneTypeList = new ArrayList<>();
        mWeightList = new ArrayList<>();
        mBodyTypeArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mBodyTypeList);
        mSkinToneTypeArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text,
                mSkinToneTypeList);
        mWeightArrayAdapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_text, mWeightList);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonSkip.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
    }

    private void initSpinners() {
        mSpinnerBodyType.setTitle("Select Body Type");
        mSpinnerSkinToneType.setTitle("Select Skin Tone Type");
        mSpinnerWeight.setTitle("Select Weight");

        mBodyTypeList.addAll(getBodyTypeList());
        mSkinToneTypeList.addAll(getSkinToneTypeList());
        mWeightList.addAll(getWeightList());

        mBodyTypeArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerBodyType.setAdapter(mBodyTypeArrayAdapter);
        mSkinToneTypeArrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        mSpinnerSkinToneType.setAdapter(mSkinToneTypeArrayAdapter);
        mWeightArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerWeight.setAdapter(mWeightArrayAdapter);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
